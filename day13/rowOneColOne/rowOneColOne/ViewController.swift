//
//  ViewController.swift
//  rowOneColOne
//
//  Created by lazar.velimirovic on 5/13/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeCorners()
    }
    @IBOutlet var fbImage: UIImageView!
    @IBOutlet var twitterImage: UIImageView!
    @IBOutlet var signUpBtn: UIButton!
    @IBOutlet var signInBtn: UIButton!
    
    
    
    func makeCorners() {
        fbImage.layer.cornerRadius = 0.5 * fbImage.bounds.size.width
        twitterImage.layer.cornerRadius = 0.5 * twitterImage.bounds.size.width
        
        signUpBtn.layer.cornerRadius = 15
        signUpBtn.layer.masksToBounds = true
        signInBtn.layer.cornerRadius = 15
        signInBtn.layer.masksToBounds = true
    }

}

