//
//  ProfileVC.swift
//  aboutMe
//
//  Created by lazar.velimirovic on 5/25/21.
//

import UIKit

class ProfileVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func goGithub(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://github.com/lazvel")! as URL, options: [:], completionHandler: nil)
    }
    @IBAction func goLinkedin(_ sender: UIButton) {
        UIApplication.shared.open(URL(string: "https://www.linkedin.com/in/lazar-velimirovic-8301a31a1/")! as URL, options: [:], completionHandler: nil)
    }
    @IBAction func goEmail(_ sender: Any) {
        makeAlert("Email", "lazar.velimirovic@eng.it")
    }
    

    func makeAlert(_ title: String,_ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .blue
        alert.addAction(UIAlertAction(title: "Thanks", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
