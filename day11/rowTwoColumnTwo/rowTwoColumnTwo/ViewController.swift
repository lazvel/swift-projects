//
//  ViewController.swift
//  rowTwoColumnTwo
//
//  Created by lazar.velimirovic on 5/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setRadius()
        makeBorderBottoms()
    }
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var loginButton: UIButton!
    @IBOutlet var facebookBtn: UIButton!
    @IBOutlet var googleBtn: UIButton!
    
    func setRadius() {
        loginButton.layer.cornerRadius = 15
        facebookBtn.layer.cornerRadius = 20
        facebookBtn.clipsToBounds = true
        googleBtn.layer.cornerRadius = 20
        googleBtn.clipsToBounds = true
    }
    
    func makeBorderBottoms() {
        let bottomLine1 = CALayer()
        let bottomLine2 = CALayer()
        bottomLine1.frame = CGRect(x: 0.0, y: 25 - 1, width: 310, height: 1.2)
        bottomLine1.backgroundColor = UIColor.white.cgColor
        bottomLine2.frame = CGRect(x: 0.0, y: 25 - 1, width: 310, height: 1.2)
        bottomLine2.backgroundColor = UIColor.white.cgColor
        usernameField.borderStyle = UITextField.BorderStyle.none
        passwordField.borderStyle = UITextField.BorderStyle.none
        usernameField.layer.addSublayer(bottomLine1)
        passwordField.layer.addSublayer(bottomLine2)
    }

}

