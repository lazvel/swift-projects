//
//  ViewController.swift
//  rowOneColTwo
//
//  Created by lazar.velimirovic on 5/11/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        emailField.layer.shadowOpacity = 1
        emailField.layer.shadowRadius = 4.0
        emailField.layer.shadowColor = UIColor.black.cgColor
        emailField.layer.borderColor = UIColor.black.withAlphaComponent(0.25).cgColor
        emailField.layer.shadowOffset = CGSize(width: 0, height: 3)
        
//        emailField.layer.cornerRadius = 12
//        emailField.clipsToBounds = true

        
//        passwordField.layer.cornerRadius = 12
//        passwordField.clipsToBounds = true
        passwordField.layer.shadowOpacity = 1
        passwordField.layer.shadowRadius = 4.0
        passwordField.layer.borderColor = UIColor.black.withAlphaComponent(0.25).cgColor
        passwordField.layer.shadowOffset = CGSize(width: 0, height: 3)
        passwordField.layer.shadowColor = UIColor.black.cgColor
        
        
        
        signInButton.layer.cornerRadius = 12
        signInButton.layer.shadowOpacity = 1
        signInButton.layer.shadowRadius = 4.0
        signInButton.layer.shadowOffset = CGSize(width: 0, height: 3)
        signInButton.layer.shadowColor = UIColor.black.cgColor
        //signInButton.clipsToBounds = true
    }

    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var signInButton: UIButton!
    
}

