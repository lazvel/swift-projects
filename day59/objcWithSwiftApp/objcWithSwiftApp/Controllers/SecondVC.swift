//
//  SecondVC.swift
//  objcWithSwiftApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

import UIKit

class SecondVC: UIViewController {
    
    var firstVC: ViewController!

    override func viewDidLoad() {
        super.viewDidLoad()

        firstVC.view.backgroundColor = .black
    }
    

}
