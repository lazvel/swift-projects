//
//  ViewController.m
//  objcWithSwiftApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import "ViewController.h"
#import "objcWithSwiftApp-Swift.h"

@interface ViewController ()
@property(nonatomic, strong) SecondVC *secondVC;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)loadSecondVC:(id)sender {
    _secondVC = [[SecondVC alloc] init];
    _secondVC.view.backgroundColor = [UIColor redColor];
    [self presentViewController:_secondVC animated:YES completion:nil];
}


@end
