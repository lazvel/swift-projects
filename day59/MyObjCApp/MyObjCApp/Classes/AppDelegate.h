//
//  AppDelegate.h
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    NSString *selectedURL;
}

@property (strong, nonatomic) NSString *selectedURL;

@end

