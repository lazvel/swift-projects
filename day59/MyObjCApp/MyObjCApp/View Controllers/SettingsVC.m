//
//  SettingsVC.m
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import "SettingsVC.h"

@interface SettingsVC ()

@end

@implementation SettingsVC
@synthesize slVol, lbVol, sgDiff, lbDiff;
@synthesize txtFieldName, txtFieldEmail, lbName, lbEmail;

// get rid of the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    return [textField resignFirstResponder];
}

- (IBAction)saveData:(id)sender {
    lbName.text = txtFieldName.text;
    [lbEmail setText:txtFieldEmail.text]; // bypass get and set
    //[lbName setText:[txtFieldName text]]; // getter and setter
}

- (IBAction)sliderValueChanged:(id)sender {
    [self updateSliderLabel];
}

- (void)updateSliderLabel {
    float vol = slVol.value;
    NSString *strVol = [NSString stringWithFormat:@"%.2f", vol];
    lbVol.text = strVol;
}

- (void)updateSegmentLabel {
    NSString *str = [sgDiff titleForSegmentAtIndex:sgDiff.selectedSegmentIndex];
    NSString *textLabel = [NSString stringWithFormat:@"Difficulty: %@", str];
    lbDiff.text = textLabel;
}

-(IBAction)segmentDidChanged:(id)sender {
    [self updateSegmentLabel];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateSliderLabel];
    [self updateSegmentLabel];
}


@end
