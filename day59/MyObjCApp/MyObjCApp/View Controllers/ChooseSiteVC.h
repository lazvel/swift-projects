//
//  ChooseSiteVC.h
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChooseSiteVC : UIViewController<UITableViewDataSource, UITableViewDelegate> {
    NSArray *listData;
    NSArray *siteData;
    NSArray *imageData; // NSMuttableArray - onaj koji bi se menjao
}

@property (nonatomic, strong) NSArray *listData;
@property (nonatomic, strong) NSArray *siteData;
@property (nonatomic, strong) NSArray *imageData;

@end

NS_ASSUME_NONNULL_END
