//
//  ViewSiteVC.m
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import "ViewSiteVC.h"
#import "AppDelegate.h"

@interface ViewSiteVC ()

@end

@implementation ViewSiteVC
@synthesize webView, activity;

// MARK: Support methods
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    [activity setHidden:NO];
    [activity startAnimating];
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    [activity setHidden:YES];
    [activity stopAnimating];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    AppDelegate *mainDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    NSURL *urlAddress = [NSURL URLWithString: mainDelegate.selectedURL];
    NSURLRequest *url = [NSURLRequest requestWithURL:urlAddress];
    [webView loadRequest:url];
    webView.navigationDelegate = self;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
