//
//  ChooseSiteVC.m
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import "ChooseSiteVC.h"
#import "SiteCell.h"
#import "AppDelegate.h"

@interface ChooseSiteVC ()

@end

@implementation ChooseSiteVC
@synthesize listData, siteData, imageData;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    listData = [NSArray arrayWithObjects:@"Audi A1", @"Audi A2", @"Audi A3", @"Audi A4", @"Audi A5", @"Audi A6", nil];
    
    imageData = [NSArray arrayWithObjects:@"1.jpeg", @"2.jpeg", @"3.jpeg", @"4.jpeg", @"5.jpeg", @"6.jpeg", nil];
    
    siteData = [NSArray arrayWithObjects:@"https://myaudi.it/it/live/", @"https://myaudi.it/it/live/audi-sport-delight-driving-experience/", @"https://myaudi.it/it/live/audi-summer-season/rs-e-tron-gt-drive/", @"https://myaudi.it/it/live/audi-summer-season/", @"https://myaudi.it/it/live/audi-summer-season/maratona-delle-dolomiti/", @"https://www.audi.it/it/web/it/modelli/q4-e-tron/q4-e-tron.html", nil];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return listData.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    SiteCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[SiteCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    cell.primaryLabel.text = [listData objectAtIndex:indexPath.row];
    cell.secondaryLabel.text = [siteData objectAtIndex:indexPath.row];
    cell.myImageView.image = [UIImage imageNamed:[imageData objectAtIndex:indexPath.row]];
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (IBAction)unwindToChooseVC:(UIStoryboardSegue *)sender {
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AppDelegate *mainDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    mainDelegate.selectedURL = [siteData objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"openSite" sender:self];
}

@end
