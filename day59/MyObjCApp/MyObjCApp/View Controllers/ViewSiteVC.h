//
//  ViewSiteVC.h
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewSiteVC : UIViewController<WKNavigationDelegate> {
    IBOutlet WKWebView *webView;
    IBOutlet UIActivityIndicatorView *activity;
}

@property (nonatomic, strong) WKWebView *webView;
@property (nonatomic, strong) UIActivityIndicatorView *activity;

@end

NS_ASSUME_NONNULL_END
