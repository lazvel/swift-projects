//
//  SettingsVC.h
//  MyObjCApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SettingsVC : UIViewController<UITextFieldDelegate> {
    IBOutlet UISlider *slVol;
    IBOutlet UILabel *lbVol;
    
    IBOutlet UISegmentedControl *sgDiff;
    IBOutlet UILabel *lbDiff;
    
    IBOutlet UITextField *txtFieldName;
    IBOutlet UITextField *txtFieldEmail;
    
    IBOutlet UILabel *lbName;
    IBOutlet UILabel *lbEmail;
}
@property (nonatomic, strong) IBOutlet UISlider *slVol;
@property (nonatomic, strong) IBOutlet UILabel *lbVol;

@property (nonatomic, strong) IBOutlet UISegmentedControl *sgDiff;
@property (nonatomic, strong) IBOutlet UILabel *lbDiff;

@property (nonatomic, strong) IBOutlet UITextField *txtFieldName;
@property (nonatomic, strong) IBOutlet UITextField *txtFieldEmail;

@property (nonatomic, strong) IBOutlet UILabel *lbName;
@property (nonatomic, strong)  IBOutlet UILabel *lbEmail;

@end

NS_ASSUME_NONNULL_END
