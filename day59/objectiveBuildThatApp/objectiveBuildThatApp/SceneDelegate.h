//
//  SceneDelegate.h
//  objectiveBuildThatApp
//
//  Created by lazar.velimirovic on 7/16/21.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

