//
//  FanFavoritesCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/21/21.
//

import UIKit

class FanFavoritesCell: UICollectionViewCell {
    
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieYear: UILabel!
    @IBOutlet var duration: UILabel!
    @IBOutlet var movieRating: UILabel!
    
    @IBOutlet var fanFavoritesBtn: UIButton!
    @IBOutlet var watchlistBtn: UIButton!
}
