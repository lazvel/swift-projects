//
//  BannerCollectionCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/17/21.
//

import UIKit

class BannerCollectionCell: UICollectionViewCell {
    
    @IBOutlet var movieTitle: UILabel!
    @IBOutlet var videoBanner: UIImageView!
    @IBOutlet var imageBanner: UIImageView!
    @IBOutlet var bannerDescription: UILabel!
    
    @IBOutlet var bannerBtn: UIButton!
}
