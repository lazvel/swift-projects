//
//  TopPicksForYouCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/18/21.
//

import UIKit

class TopPicksForYouCell: UICollectionViewCell {
    
    @IBOutlet var poster: UIImageView!
    @IBOutlet var rating: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var year: UILabel!
    @IBOutlet var duration: UILabel!
    
    @IBOutlet var topPicksBtn: UIButton!
    @IBOutlet var watchlistBtn: UIButton!
    
}
