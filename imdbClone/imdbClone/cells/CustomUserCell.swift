//
//  CustomUserCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/16/21.
//

import UIKit

class CustomUserCell: UICollectionViewCell {
    
    @IBOutlet var cellLabel: UILabel!
    @IBOutlet var labelName: UILabel!
    @IBOutlet var labelValue: UILabel!
}
