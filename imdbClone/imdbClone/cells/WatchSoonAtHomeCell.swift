//
//  WatchSoonAtHomeCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/22/21.
//

import UIKit

class WatchSoonAtHomeCell: UICollectionViewCell {
    
    @IBOutlet var seriesPoster: UIImageView!
    @IBOutlet var seriesName: UILabel!
    @IBOutlet var seriesYear: UILabel!
    
    @IBOutlet var watchAtHomeBtn: UIButton!
}
