//
//  ComingSoonCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/21/21.
//

import UIKit

class ComingSoonCell: UICollectionViewCell {
    
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieRating: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieYear: UILabel!
    @IBOutlet var movieDuration: UILabel!
    
    @IBOutlet var comingSoonBtn: UIButton!
}
