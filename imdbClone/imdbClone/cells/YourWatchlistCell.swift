//
//  YourWatchlistCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/23/21.
//

import UIKit

class YourWatchlistCell: UICollectionViewCell {
    
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var movieYear: UILabel!
}
