//
//  MostPopularMoviesCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/23/21.
//

import UIKit

class MostPopularMoviesCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var movieName: UILabel!
    @IBOutlet var moviePoster: UIImageView!
    @IBOutlet var movieYear: UILabel!
    @IBOutlet var movieRuntime: UILabel!
    @IBOutlet var movieRated: UILabel!
    @IBOutlet var imdbRating: UILabel!
    @IBOutlet var movieMetascore: UILabel!
    
    @IBOutlet var addMovieBtn: UIButton!
    
}
