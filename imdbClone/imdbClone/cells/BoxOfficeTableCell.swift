//
//  BoxOfficeTableCell.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/22/21.
//

import UIKit

class BoxOfficeTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var rowNumber: UILabel!
    @IBOutlet var movieName: UILabel!
    @IBOutlet var boxOfficeLabel: UILabel!
    
}
