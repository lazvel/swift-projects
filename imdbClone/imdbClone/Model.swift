//
//  Model.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/16/21.
//

import Foundation

struct UserPosibility {
    var centerL: String
    var bellowCenter: String
    var actionCount: Int
}

struct SearchMovies: Codable{
    var search: [MovieShort]
    var totalResults: String
    var response: String
    
    enum CodingKeys: String, CodingKey {
        case search = "Search"
        case totalResults
        case response = "Response"
    }
}

struct MovieShort: Codable{
    var title: String
    var year: String
    var imdbID: String
    var type: String
    var poster: String
    
    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case imdbID
        case type = "Type"
        case poster = "Poster"
    }
}

struct Rating: Codable {
    var source: String
    var value: String
    
    enum CodingKeys: String, CodingKey {
        case source = "Source"
        case value = "Value"
    }
}

struct MovieDetailsShort: Codable {
    var title: String
    var boxOffice: String? = "Not Provided"
    
    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case boxOffice = "BoxOffice"
    }
}

struct MovieDetails: Codable {
    var title: String
    var year: String
    var rated: String
    var released: String
    var runtime: String
    var genre:  String
    var director: String
    var writer: String
    var actors: String
    var plot: String
    var language:  String
    var country: String
    var awards: String
    var poster: String
    var ratings: [Rating]
    var metascore: String?
    var imdbRating: String
    var imdbVotes: String
    var imdbID: String
    var type: String
    var DVD: String
    var boxOffice: String
    var production: String
    var website: String
    var response: String
    
    enum CodingKeys: String, CodingKey {
        case title = "Title"
        case year = "Year"
        case rated = "Rated"
        case released = "Released"
        case runtime = "Runtime"
        case genre =  "Genre"
        case director = "Director"
        case writer = "Writer"
        case actors = "Actors"
        case plot = "Plot"
        case language = "Language"
        case country = "Country"
        case awards = "Awards"
        case poster = "Poster"
        case ratings = "Ratings"
        case metascore = "Metascore"
        case imdbRating
        case imdbVotes
        case imdbID
        case type = "Type"
        case DVD
        case boxOffice = "BoxOffice"
        case production = "Production"
        case website = "Website"
        case response = "Response"
    }
}


