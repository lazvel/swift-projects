//
//  DataProcessing.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/16/21.
//

import Foundation

class DataProcessing {
    static var userPosibilities: [UserPosibility] = [
        UserPosibility(centerL: "Rate a show or a movie", bellowCenter: "Ratings", actionCount: 0),
        UserPosibility(centerL: "Create a list", bellowCenter: "Lists", actionCount: 0),
        UserPosibility(centerL: "No reviews", bellowCenter: "Reviews", actionCount: 0)
    ]
    
    static var page: Int = 1
    
    static var moviesArr: [MovieShort] = []
    static var singleMovie: SearchMovies?
    static var moviesArr2: [MovieShort] = []
    static var moviesFanFav: [MovieShort] = []
    static var moviesStreamingArr: [MovieShort] = []
    static var moviesInTheaterArr: [MovieShort] = []
    static var moviesComingSoon: [MovieShort] = []
    static var moviesWatchHome: [MovieShort] = []
    
    static var movieDetail: MovieDetails?
    static var movieDetails: [MovieDetails] = []
    
    
    static var apiStr = "https://www.omdbapi.com/?apikey=4b92f7fd&s=batman&page=" + String(page)
    static var apiStr2 = "https://www.omdbapi.com/?apikey=4b92f7fd&s=asterix"
    static var apiStrTop = "https://www.omdbapi.com/?apikey=4b92f7fd&s=avengers&type=movie"
    static var movieStr = "https://www.omdbapi.com/?apikey=4b92f7fd&i=tt0133385"
    static var theaterStr = "https://www.omdbapi.com/?apikey=4b92f7fd&s=Of%20The&type=movie&y=2021"
    static var comingSoonStr = "https://www.omdbapi.com/?apikey=4b92f7fd&s=Book&type=movie&y=2021"
    static var watchAtHomeStr = "https://www.omdbapi.com/?apikey=4b92f7fd&s=Story&y=2021&type=series"
    
    static var ratings: [String] = ["7.2", "5.3", "7.5", "8.3", "5.9", "9.1", "8.1", "7.1", "7.8", "8.4"]
        
    static func fetchMovie(url: String, completion: @escaping (SearchMovies?) -> Void) {
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL) { (data, response, error) in
            if let data = data, case let movies = try? JSONDecoder().decode(SearchMovies.self, from: data) {
                completion(movies)
            }
        }
        task.resume()
    }
    static func fetchMovieDetails(url: String, completion: @escaping (MovieDetails?) -> Void) {
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL) { (data, response, error) in
            if let data = data, case let movieInfo = try? JSONDecoder().decode(MovieDetails.self, from: data) {
                completion(movieInfo)
            }
        }
        task.resume()
    }
    
    static func fetchMovieDetailsShort(url: String, completion: @escaping (MovieDetailsShort?) -> Void) {
        guard let apiURL = URL(string: url) else { return }
        let task = URLSession.shared.dataTask(with: apiURL) { (data, response, error) in
            if let data = data, case let movieInfo = try? JSONDecoder().decode(MovieDetailsShort.self, from: data) {
                completion(movieInfo)
            }
        }
        task.resume()
    }
}
