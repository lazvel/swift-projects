//
//  Constants.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/28/21.
//

import Foundation

struct Constants {
    struct  Storyboard {
        static let homeViewController = "HomeVC"
    }
}
