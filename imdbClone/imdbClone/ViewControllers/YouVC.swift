//
//  ViewController.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/15/21.
//

import UIKit

class YouVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    //var signInVC = SignInVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customUserView.isHidden = true
        self.view.bringSubviewToFront(noUserView)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        customUserView.isHidden = false
        self.view.bringSubviewToFront(customUserView)
    }
    
    @IBOutlet var noUserView: UIView!
    @IBOutlet var customUserView: UIView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        DataProcessing.userPosibilities.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "customUserCell", for: indexPath) as! CustomUserCell
        cell.layer.cornerRadius = 10
        cell.cellLabel.text = DataProcessing.userPosibilities[indexPath.row].centerL
        cell.labelName.text = DataProcessing.userPosibilities[indexPath.row].bellowCenter
        cell.labelValue.text = String( DataProcessing.userPosibilities[indexPath.row].actionCount)
        cell.cellLabel.layer.cornerRadius = 7
        cell.cellLabel.layer.masksToBounds = true
        
        return cell
    }

}

