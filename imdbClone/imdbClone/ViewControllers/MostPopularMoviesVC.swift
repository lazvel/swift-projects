//
//  MostPopularMoviesVC.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/23/21.
//

import UIKit

class MostPopularMoviesVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
   
    var popularMovies: [MovieDetails] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(popularMovies.count)
        numberOfResultsLabel.text = String(popularMovies.count)
        
//        DispatchQueue.main.async {
//            self.popularMoviesTable.reloadData()
//        }
    }
    
    @IBOutlet var popularMoviesTable: UITableView!
    @IBOutlet var numberOfResultsLabel: UILabel!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return popularMovies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mostPopularCell", for: indexPath) as! MostPopularMoviesCell
        
        if let urlString = popularMovies[indexPath.row].poster as? String,
           let url = URL(string: urlString) {
               URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                   if let data = data {
                    DispatchQueue.main.async {
                        cell.moviePoster.image = UIImage(data: data)
                    }
                   }
               }.resume()
           }
        
        cell.movieName.text = popularMovies[indexPath.row].title
        cell.movieYear.text = popularMovies[indexPath.row].year
        cell.movieRuntime.text = popularMovies[indexPath.row].runtime
        cell.movieRated.text = popularMovies[indexPath.row].rated
        cell.imdbRating.text = popularMovies[indexPath.row].imdbRating
        cell.movieMetascore.text = popularMovies[indexPath.row].metascore
        
        colorMetascore(cell.movieMetascore)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 168
    }
    
    func colorMetascore(_ label: UILabel) {
        for item in popularMovies {
            if let myInt = Int(item.metascore!) {
                if myInt <= 60 && myInt > 40 {
                    label.layer.backgroundColor = UIColor.systemYellow.cgColor
                    label.layer.masksToBounds = true
                } else if myInt <= 40 && myInt > 0 {
                    label.layer.backgroundColor = UIColor.red.cgColor
                    label.layer.masksToBounds = true
                } else if myInt > 60 {
                    label.layer.backgroundColor = UIColor.systemGreen.cgColor
                    label.layer.masksToBounds = true
                } else {
                    label.text = "N"
                }
            }
            
        }
    }

}
