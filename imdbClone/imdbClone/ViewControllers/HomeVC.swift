//
//  HomeVC.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/15/21.
//

import UIKit

class HomeVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var data = DataProcessing()
    var movies: [MovieShort] = []
    var movieDetails: [MovieDetails] = []
    var movieIds: [String] = []
    var movieStr = "https://www.omdbapi.com/?apikey=4b92f7fd&i="
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        corneringLabels()
        
        prepareDataForBanner()
        
        makeBorders()
    }
    // MARK: Preparing Data - Fetching
     func prepareDataForBanner() {
        DataProcessing.fetchMovie(url: DataProcessing.apiStr) { (result) in
            //print("Before Guard")
            guard let result = result else { return }
            //print("Result \(result)")
            for item in result.search {
                DataProcessing.moviesArr.append(item)
                DataProcessing.moviesStreamingArr.append(item)
            }
            
            for item in DataProcessing.moviesStreamingArr {
                self.movieIds.append(item.imdbID)
                
            }
            
            for item in self.movieIds {
                DataProcessing.fetchMovieDetails(url: self.movieStr + item) { (result) in
                    //print("Before Guard")
                    guard let result = result else { return }
                    //print("Rezultat \(result)")
                    self.movieDetails.append(result)
                    //print("Print za iddetails \(self.movieDetails)")
                    DispatchQueue.main.async {
                        self.boxOfficeTable.reloadData()
                    }
                }
            }
            
            DataProcessing.singleMovie = result
            DispatchQueue.main.async {
                self.bannerCollection.reloadData()
                self.topPicksForYouCollection.reloadData()
                self.exploreStreamingCollection.reloadData()
                self.boxOfficeTable.reloadData()
            }
            DataProcessing.page += 1
        }
        
        DataProcessing.fetchMovie(url: DataProcessing.apiStr2) { (result) in
            guard let result = result else { return }
            for item in result.search {
                //DataProcessing.moviesArr.append(item)
                DataProcessing.moviesArr2.append(item)
            }
            DispatchQueue.main.async {
                self.bannerCollection.reloadData()
                self.topPicksForYouCollection.reloadData()
            }
        }
        
        DataProcessing.fetchMovie(url: DataProcessing.apiStrTop) { (result) in
            guard let result = result else { return }
            for item in result.search {
                DataProcessing.moviesFanFav.append(item)
            }
            DispatchQueue.main.async {
                self.fanFavoritesCollection.reloadData()
            }
        }
        
        DataProcessing.fetchMovie(url: DataProcessing.theaterStr) { (result) in
            guard let result = result else { return }
            for item in result.search {
                DataProcessing.moviesInTheaterArr.append(item)
            }
            DispatchQueue.main.async {
                self.inTheatersCollection.reloadData()
            }
        }
        
        DataProcessing.fetchMovie(url: DataProcessing.comingSoonStr) { (result) in
            guard let result = result else { return }
            for item in result.search {
                DataProcessing.moviesComingSoon.append(item)
            }
            DispatchQueue.main.async {
                self.comingSoonCollection.reloadData()
            }
        }
        
        DataProcessing.fetchMovie(url: DataProcessing.watchAtHomeStr) { (result) in
            guard let result = result else { return }
            for item in result.search {
                DataProcessing.moviesWatchHome.append(item)
            }
            DispatchQueue.main.async {
                self.watchSoonAtHomeCollection.reloadData()
            }
        }
        
        DataProcessing.fetchMovieDetails(url: DataProcessing.movieStr) { (result) in
            guard let result = result else { return }
            DataProcessing.movieDetail = result
            DispatchQueue.main.async {
                self.bannerCollection.reloadData()
                self.topPicksForYouCollection.reloadData()
            }
        }
        
    }
 
    @IBOutlet var emptyWatchListView: UIView!
    
    @IBOutlet var bannerCollection: UICollectionView!
    @IBOutlet var topPicksForYouCollection: UICollectionView!
    @IBOutlet var fanFavoritesCollection: UICollectionView!
    @IBOutlet var exploreStreamingCollection: UICollectionView!
    @IBOutlet var inTheatersCollection: UICollectionView!
    @IBOutlet var comingSoonCollection: UICollectionView!
    @IBOutlet var boxOfficeTable: UITableView!
    @IBOutlet var watchSoonAtHomeCollection: UICollectionView!
    
    @IBOutlet var yellowLabel: UILabel!
    @IBOutlet var yellowLabelTwo: UILabel!
    @IBOutlet var yellowLabelThree: UILabel!
    @IBOutlet var yellowLabelFour: UILabel!
    @IBOutlet var yellowLabelFive: UILabel!
    @IBOutlet var yellowLabelSix: UILabel!
    @IBOutlet var yellowLabelSeven: UILabel!
    @IBOutlet var yellowLabelEight: UILabel!
    
    
    func corneringLabels() {
        yellowLabel.layer.cornerRadius = 3
        yellowLabel.layer.masksToBounds = true
        
        yellowLabelTwo.layer.cornerRadius = 3
        yellowLabelTwo.layer.masksToBounds = true
    
        yellowLabelThree.layer.cornerRadius = 3
        yellowLabelThree.layer.masksToBounds = true
        
        yellowLabelFour.layer.cornerRadius = 3
        yellowLabelFour.layer.masksToBounds = true
    
        yellowLabelFive.layer.cornerRadius = 3
        yellowLabelFive.layer.masksToBounds = true
        
        yellowLabelSix.layer.cornerRadius = 3
        yellowLabelSix.layer.masksToBounds = true
        
        yellowLabelSeven.layer.cornerRadius = 3
        yellowLabelSeven.layer.masksToBounds = true
    
        yellowLabelEight.layer.cornerRadius = 3
        yellowLabelEight.layer.masksToBounds = true
    }
    //MARK: STYLIZING CELL
    func cellStylezing(_ cell: UICollectionViewCell, _ button: UIButton?) {
        cell.layer.cornerRadius = 15.0
        cell.layer.borderWidth = 0.0
        //cell2.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.shadowColor = UIColor.black.cgColor
        cell.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        cell.layer.shadowRadius = 5.0
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false
        
        button?.layer.borderWidth = 0.5
        button?.layer.borderColor = UIColor.lightGray.cgColor
        button?.layer.cornerRadius = 3
        button?.layer.masksToBounds = true
    }
    
    // MARK: WORK WITH COLLECTION
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == bannerCollection {
            return DataProcessing.moviesArr.count
        } else if collectionView == topPicksForYouCollection {
            return DataProcessing.moviesArr2.count
        } else if collectionView == fanFavoritesCollection {
            return DataProcessing.moviesFanFav.count
        } else if collectionView == exploreStreamingCollection {
            return DataProcessing.moviesStreamingArr.count
        } else if collectionView == inTheatersCollection {
            return DataProcessing.moviesInTheaterArr.count
        } else if collectionView == comingSoonCollection {
            return DataProcessing.moviesComingSoon.count
        } else if collectionView == watchSoonAtHomeCollection {
            return DataProcessing.moviesWatchHome.count
        } else {
            return 0
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == bannerCollection {
            let cell = bannerCollection.dequeueReusableCell(withReuseIdentifier: "mainBannerCell", for: indexPath) as! BannerCollectionCell
            
            // MARK: FETCHING IMAGE
            if let urlString = DataProcessing.moviesArr[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                           URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                               if let data = data {
                                DispatchQueue.main.async {
                                    cell.videoBanner.image = UIImage(data: data)
                                    cell.imageBanner.image = UIImage(data: data)
                                }
                               }
                           }.resume()
                       }
            cell.movieTitle.text = DataProcessing.moviesArr[indexPath.row].title
            cell.bannerDescription.text = DataProcessing.moviesArr[indexPath.row].year
            
            return cell
        } else if collectionView == topPicksForYouCollection {
            let cell2 = topPicksForYouCollection.dequeueReusableCell(withReuseIdentifier: "topPicksForYou", for: indexPath) as! TopPicksForYouCell

            cellStylezing(cell2, cell2.watchlistBtn)
            
            // MARK: FETCHING IMAGE
            if let urlString = DataProcessing.moviesArr2[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell2.poster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            
            cell2.movieName.text = DataProcessing.moviesArr2[indexPath.row].title
            cell2.rating.text = DataProcessing.ratings[indexPath.row]
            cell2.year.text = DataProcessing.moviesArr2[indexPath.row].year
            //cell2.duration.text = DataProcessing.movieDetail?.runtime
            
            return cell2
        } else if collectionView == fanFavoritesCollection {
            let cell3 = fanFavoritesCollection
                .dequeueReusableCell(withReuseIdentifier: "fanFavoritesCell", for: indexPath) as! FanFavoritesCell
            
            cellStylezing(cell3, cell3.watchlistBtn)
            
            if let urlString = DataProcessing.moviesFanFav[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell3.moviePoster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            DataProcessing.ratings.shuffle()
            cell3.movieRating.text = DataProcessing.ratings[indexPath.row]
            cell3.movieName.text = DataProcessing.moviesFanFav[indexPath.row].title
            cell3.movieYear.text = DataProcessing.moviesFanFav[indexPath.row].year
            return cell3
        } else if collectionView == exploreStreamingCollection {
            let cell4 = exploreStreamingCollection
                .dequeueReusableCell(withReuseIdentifier: "exploreStreamingCell", for: indexPath) as! ExploreStreamingCell
            
            cellStylezing(cell4, cell4.watchNowBtn)
            
            if let urlString = DataProcessing.moviesStreamingArr[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell4.moviePoster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            DataProcessing.ratings.shuffle()
            cell4.movieRating.text = DataProcessing.ratings[indexPath.row]
            cell4.movieName.text = DataProcessing.moviesStreamingArr[indexPath.row].title
            cell4.movieYear.text = DataProcessing.moviesStreamingArr[indexPath.row].year
            return cell4
        } else if collectionView == inTheatersCollection{
            let cell5 = inTheatersCollection
                .dequeueReusableCell(withReuseIdentifier: "inTheatersCell", for: indexPath) as! InTheatersCell
            
            cellStylezing(cell5, cell5.showtimesBtn)
            
            if let urlString = DataProcessing.moviesInTheaterArr[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell5.moviePoster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            DataProcessing.ratings.shuffle()
            cell5.movieRating.text = DataProcessing.ratings[indexPath.row]
            cell5.movieName.text = DataProcessing.moviesInTheaterArr[indexPath.row].title
            cell5.movieYear.text = DataProcessing.moviesInTheaterArr[indexPath.row].year
            return cell5
        } else if collectionView == comingSoonCollection {
            let cell6 = comingSoonCollection
                .dequeueReusableCell(withReuseIdentifier: "comingSoonCell", for: indexPath) as! ComingSoonCell
            
            cellStylezing(cell6, nil)
            
            if let urlString = DataProcessing.moviesComingSoon[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell6.moviePoster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            DataProcessing.ratings.shuffle()
            cell6.movieRating.text = DataProcessing.ratings[indexPath.row]
            cell6.movieName.text = DataProcessing.moviesComingSoon[indexPath.row].title
            cell6.movieYear.text = DataProcessing.moviesComingSoon[indexPath.row].year
            return cell6
        } else {
            let cell7 = watchSoonAtHomeCollection
                .dequeueReusableCell(withReuseIdentifier: "watchAtHomeCell", for: indexPath) as! WatchSoonAtHomeCell
            
            cellStylezing(cell7, nil)
            
            if let urlString = DataProcessing.moviesWatchHome[indexPath.row].poster as? String,
               let url = URL(string: urlString) {
                   URLSession.shared.dataTask(with: url) { (data, urlResponse, error) in
                       if let data = data {
                        DispatchQueue.main.async {
                            cell7.seriesPoster.image = UIImage(data: data)
                        }
                       }
                   }.resume()
               }
            cell7.seriesName.text = DataProcessing.moviesWatchHome[indexPath.row].title
            cell7.seriesYear.text = DataProcessing.moviesWatchHome[indexPath.row].year
            
            return cell7
        }
        
    }
    var watchlistArr: [MovieShort] = []
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Usao u select")
        print(DataProcessing.moviesArr[indexPath.row])
//        if bannerBtn.isTouchInside || topPicksBtn.isTouchInside {
//            watchlistArr.append(DataProcessing.moviesArr[indexPath.row])
//        }
        
    }
    // MARK: Table Work
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "boxOfficeCell") as! BoxOfficeTableCell
        
        cell.rowNumber.text = String(indexPath.row + 1)
        if !movieDetails.isEmpty {
            cell.movieName.text = movieDetails[indexPath.row].title
            if movieDetails[indexPath.row].boxOffice == "N/A" {
                cell.boxOfficeLabel.text = "Not provided"
            } else {
                cell.boxOfficeLabel.text = movieDetails[indexPath.row].boxOffice
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 92
    }
    
    // MARK: SEGUES
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "browsePopularMovies" {
            let controller = segue.destination as! MostPopularMoviesVC
            
            controller.popularMovies = movieDetails
        }
    }
    
    //MARK: BORDERS MAKING
    @IBOutlet var browsePopularMoviesBtn: UIButton!
    func makeBorders() {
        browsePopularMoviesBtn.layer.borderWidth = 0.5
        browsePopularMoviesBtn.layer.borderColor = UIColor.lightGray.cgColor
        browsePopularMoviesBtn.layer.cornerRadius = 3
    }
}
// MARK: Setting the Size of Collection
extension HomeVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == bannerCollection {
            return CGSize(width: collectionView.frame.width, height: collectionView.frame.height)
        } else if collectionView == topPicksForYouCollection {
            return CGSize(width: 140, height: 380)
        } else if collectionView == watchSoonAtHomeCollection {
            return CGSize(width: 140, height: 340)
        } else {
            return CGSize(width: 140, height: 380)
        }
    }
}
