//
//  SignInVC.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/15/21.
//

import UIKit
import GoogleSignIn
import FirebaseAuth

class SignInVC: UIViewController, GIDSignInDelegate{
    
    var youVC = YouVC()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        GIDSignIn.sharedInstance().presentingViewController = self
        GIDSignIn.sharedInstance().delegate = self
    }
    @IBOutlet var signFacebook: UIButton!
    @IBOutlet var signGoogle: UIButton!
    @IBOutlet var signAmazon: UIButton!
    @IBOutlet var signImdb: UIButton!
    
    
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if checkUser() {
//            youVC.customUserView.isHidden = false
//            youVC.view.bringSubviewToFront(youVC.customUserView)
//        } else {
//            youVC.customUserView.isHidden = true
//            youVC.view.bringSubviewToFront(youVC.noUserView)
//        }
//    }
    @IBAction func googleSignInTapped(_ sender: Any) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error == nil {
            print("User id \(String(describing: user.userID))")
        } else {
            print("NO PROVIDED EMAIL: \(String(describing: error))")
            let credentials = GoogleAuthProvider.credential(withIDToken: user.authentication.idToken, accessToken: user.authentication.accessToken)
            Auth.auth().signIn(with: credentials) { (result, err) in
                if let err = err {
                    print(err.localizedDescription)
                } else {
                    print("Login succesfully")
                }
            }
        }
    }

}
