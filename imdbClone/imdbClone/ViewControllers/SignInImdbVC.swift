//
//  SignInImdbVC.swift
//  imdbClone
//
//  Created by lazar.velimirovic on 6/28/21.
//

import UIKit
import FirebaseAuth

class SignInImdbVC: UIViewController {
    
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var errorLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        errorLabel.alpha = 0
    }
    
    func validateFields() -> String? {
        if emailField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            
            return "Please fill in all fields."
        }
        // check if the pass is secure
        let cleanedPassword = passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        if Utilities.isPasswordValid(cleanedPassword) == false {
            // password isn't secure enough
            return "Please make sure your password is at least 8 characters, contains a special character and a number"
        }
        return nil
    }
    
    @IBAction func signInTapped(_ sender: Any) {
        let error = validateFields()
        
        if error != nil {
            // there's something wrong with the fields, show err message
            showError(error!)
        } else {
            // cleaned versions of the data
            let email = emailField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            // sign in
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                if error != nil {
                    self.showError("Error while signing in")
                } else {
                    let tabBarController = self.storyboard?.instantiateViewController(identifier: "TabBar")
                    self.view.window?.rootViewController = tabBarController
                    self.view.window?.makeKeyAndVisible()
                }
            }
        }
    }
    func showError(_ message: String) {
        errorLabel.text = message
        errorLabel.alpha = 1
    }
    
}

