//
//  Starship.swift
//  starship
//
//  Created by lazar.velimirovic on 6/4/21.
//

import Foundation

struct Starship: Codable {
    let name: String
    let model: String
    let manufacturer: String
    let cost_in_credits: String
    let max_atmosphering_speed: String
    let passengers: String
    let cargo_capacity: String
    let consumables: String
    let hyperdrive_rating: String
    let MGLT: String
    let starship_class: String
    let pilots: [String]
    let films: [String]
    let created: String
    let edited: String
    let url: String
}

struct FullStarship: Codable {
    let count: Int
    let next: String
    let previous: String?
    let results: [Starship]
    
//    init?(json: Any) {
//        guard let myStarships = json as? NSDictionary else {
//            return nil
//        }
//
//        self.count = 0
//        self.next = ""
//
//        self.previous = myStarships["previous"] as? String ?? ""
//        self.results = []
//    }
}
