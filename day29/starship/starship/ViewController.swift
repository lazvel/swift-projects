//
//  ViewController.swift
//  starship
//
//  Created by lazar.velimirovic on 6/4/21.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        DataProcessing.getData()
        
    }
}

