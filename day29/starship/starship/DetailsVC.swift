//
//  DetailsVC.swift
//  starship
//
//  Created by lazar.velimirovic on 6/7/21.
//

import UIKit

class DetailsVC: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var selectedShip: Starship? = nil
    
    var shipDetails: [String?] {
        get {
            return [selectedShip?.name, selectedShip?.model, selectedShip?.manufacturer, selectedShip?.cost_in_credits, selectedShip?.max_atmosphering_speed, selectedShip?.passengers, selectedShip?.cargo_capacity, selectedShip?.consumables, selectedShip?.hyperdrive_rating, selectedShip?.MGLT, selectedShip?.starship_class, selectedShip?.pilots.first, selectedShip?.films.first, selectedShip?.created, selectedShip?.edited, selectedShip?.url]
            
        }
        set {
            self.shipDetails = [selectedShip?.name, selectedShip?.model, selectedShip?.manufacturer, selectedShip?.cost_in_credits, selectedShip?.max_atmosphering_speed, selectedShip?.passengers, selectedShip?.cargo_capacity, selectedShip?.consumables, selectedShip?.hyperdrive_rating, selectedShip?.MGLT, selectedShip?.starship_class, selectedShip?.pilots.first, selectedShip?.films.first, selectedShip?.created, selectedShip?.edited, selectedShip?.url]
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = selectedShip?.name
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataProcessing.properties.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath) as! DetailsTableCell
        
        
        cell.layer.borderWidth = 2
        cell.layer.backgroundColor = UIColor.black.cgColor
        cell.leftValue.text = DataProcessing.properties[indexPath.row]
        cell.rightValue.text = shipDetails[indexPath.row]
        return cell
    }
    

}
