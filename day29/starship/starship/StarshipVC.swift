//
//  StarshipVC.swift
//  starship
//
//  Created by lazar.velimirovic on 6/7/21.
//

import UIKit

class StarshipVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    override func viewDidLoad() {
        super.viewDidLoad()

        tableVieww.reloadData()
    }
    
    @IBOutlet var tableVieww: UITableView!
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return DataProcessing.starshipArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "starshipCell", for: indexPath) as! StarshipTableCell
        
        cell.starshipName.text = DataProcessing.starshipArr[indexPath.row].name
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let controller = segue.destination as! DetailsVC
            
            controller.selectedShip = DataProcessing.starshipArr[self.tableVieww.indexPathForSelectedRow!.row]
        }
    }

}
