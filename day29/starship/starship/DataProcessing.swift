//
//  DataProcessing.swift
//  starship
//
//  Created by lazar.velimirovic on 6/4/21.
//

import Foundation

class DataProcessing {
    static let decoder = JSONDecoder()
    
    static let string =
        """
        [{
            "name": "Death Star",
            "model": "DS-1 Orbital Battle Station",
            "manufacturer": "Imperial Department of Military Research, Sienar Fleet Systems",
            "cost_in_credits": "1000000000000",
            "length": "120000",
            "max_atmosphering_speed": "n/a",
            "crew": "342,953",
            "passengers": "843,342",
            "cargo_capacity": "1000000000000",
            "consumables": "3 years",
            "hyperdrive_rating": "4.0",
            "MGLT": "10",
            "starship_class": "Deep Space Mobile Battlestation",
            "pilots": [],
            "films": [
                "http://swapi.dev/api/films/1/"
            ],
            "created": "2014-12-10T16:36:50.509000Z",
            "edited": "2014-12-20T21:26:24.783000Z",
            "url": "http://swapi.dev/api/starships/9/"
        },
        {
            "name": "Millennium Falcon",
            "model": "YT-1300 light freighter",
            "manufacturer": "Corellian Engineering Corporation",
            "cost_in_credits": "100000",
            "length": "34.37",
            "max_atmosphering_speed": "1050",
            "crew": "4",
            "passengers": "6",
            "cargo_capacity": "100000",
            "consumables": "2 months",
            "hyperdrive_rating": "0.5",
            "MGLT": "75",
            "starship_class": "Light freighter",
            "pilots": [
                "http://swapi.dev/api/people/13/",
                "http://swapi.dev/api/people/14/",
                "http://swapi.dev/api/people/25/",
                "http://swapi.dev/api/people/31/"
            ],
            "films": [
                "http://swapi.dev/api/films/1/",
                "http://swapi.dev/api/films/2/",
                "http://swapi.dev/api/films/3/"
            ],
            "created": "2014-12-10T16:59:45.094000Z",
            "edited": "2014-12-20T21:23:49.880000Z",
            "url": "http://swapi.dev/api/starships/10/"
        },
        {
            "name": "Y-wing",
            "model": "BTL Y-wing",
            "manufacturer": "Koensayr Manufacturing",
            "cost_in_credits": "134999",
            "length": "14",
            "max_atmosphering_speed": "1000km",
            "crew": "2",
            "passengers": "0",
            "cargo_capacity": "110",
            "consumables": "1 week",
            "hyperdrive_rating": "1.0",
            "MGLT": "80",
            "starship_class": "assault starfighter",
            "pilots": [],
            "films": [
                "http://swapi.dev/api/films/1/",
                "http://swapi.dev/api/films/2/",
                "http://swapi.dev/api/films/3/"
            ],
            "created": "2014-12-12T11:00:39.817000Z",
            "edited": "2014-12-20T21:23:49.883000Z",
            "url": "http://swapi.dev/api/starships/11/"
        },
        {
            "name": "CR90 corvette",
            "model": "CR90 corvette",
            "manufacturer": "Corellian Engineering Corporation",
            "cost_in_credits": "3500000",
            "length": "150",
            "max_atmosphering_speed": "950",
            "crew": "30-165",
            "passengers": "600",
            "cargo_capacity": "3000000",
            "consumables": "1 year",
            "hyperdrive_rating": "2.0",
            "MGLT": "60",
            "starship_class": "corvette",
            "pilots": [],
            "films": [
                "http://swapi.dev/api/films/1/",
                "http://swapi.dev/api/films/3/",
                "http://swapi.dev/api/films/6/"
            ],
            "created": "2014-12-10T14:20:33.369000Z",
            "edited": "2014-12-20T21:23:49.867000Z",
            "url": "http://swapi.dev/api/starships/2/"
        },
        {
            "name": "X-wing",
            "model": "T-65 X-wing",
            "manufacturer": "Incom Corporation",
            "cost_in_credits": "149999",
            "length": "12.5",
            "max_atmosphering_speed": "1050",
            "crew": "1",
            "passengers": "0",
            "cargo_capacity": "110",
            "consumables": "1 week",
            "hyperdrive_rating": "1.0",
            "MGLT": "100",
            "starship_class": "Starfighter",
            "pilots": [
                "http://swapi.dev/api/people/1/",
                "http://swapi.dev/api/people/9/",
                "http://swapi.dev/api/people/18/",
                "http://swapi.dev/api/people/19/"
            ],
            "films": [
                "http://swapi.dev/api/films/1/",
                "http://swapi.dev/api/films/2/",
                "http://swapi.dev/api/films/3/"
            ],
            "created": "2014-12-12T11:19:05.340000Z",
            "edited": "2014-12-20T21:23:49.886000Z",
            "url": "http://swapi.dev/api/starships/12/"
        }
    ]
    """
    
    static var properties = ["Name", "Model", "Manufacturer","Cost In Credits","Max Atmosphering Speed", "Passengers", "Cargo Capacity","Consumables","Hyperdrive Rating","MGLT","Starship Class","Pilots", "Films",  "Created", "Edited", "Url"]
    
    static var starshipArr: [Starship] = []
    
//    static func decodeJson() {
//        //let jsonData = string.data(using: .utf8)!
//        let jsonData2 = string2.data(using: .utf8)!
//        
//        let data: [Starship] = try! decoder.decode([Starship].self, from: jsonData2)
//        
//        for item in data {
//            starshipArr.append(item)
//        }
//
//
//        print("Nizzz \(starshipArr) KRAJ NIZA")
//        //starshipArr.append(data)
//    }
    
    static let url = URL(string: "https://swapi.dev/api/starships/")!
    static var task: URLSessionTask!
    static var photoInfo: FullStarship?
    
    
    static func getData() {
        task = URLSession.shared.dataTask(with: url) { [self] (data, response, error) in
            
            if let data = data, let photoInfo: FullStarship = try? decoder.decode(FullStarship.self, from: data) {
                print(photoInfo)
                for item in photoInfo.results {
                    DataProcessing.starshipArr.append(item)
                }
            }
        }
        task.resume()
    }
    
    

}
