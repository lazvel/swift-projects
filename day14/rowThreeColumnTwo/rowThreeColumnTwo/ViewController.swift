//
//  ViewController.swift
//  rowThreeColumnTwo
//
//  Created by lazar.velimirovic on 5/14/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeCorners()
    }
    @IBOutlet var playBtn: UIButton!
    @IBOutlet var shareBtn: UIButton!
    @IBOutlet var addBtn: UIButton!
    @IBOutlet var songHolder: UIView!
    
    
    func makeCorners() {
        songHolder.layer.cornerRadius = 10
        playBtn.layer.cornerRadius = 0.5 * playBtn.bounds.size.width
        playBtn.layer.masksToBounds = true
        
        shareBtn.layer.cornerRadius = 15
        
        addBtn.layer.cornerRadius = 0.5 * addBtn.bounds.size.width
        
    }

}

