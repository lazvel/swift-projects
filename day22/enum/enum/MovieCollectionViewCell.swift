//
//  MovieCollectionViewCell.swift
//  enum
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet var actionPoster: UIImageView!
    @IBOutlet var movieName: UILabel!
    
    @IBOutlet var comedyPoster: UIImageView!
    @IBOutlet var comedyName: UILabel!
    
    @IBOutlet var thrillerPoster: UIImageView!
    @IBOutlet var thrillerName: UILabel!
    
    @IBOutlet var dramaPoster: UIImageView!
    @IBOutlet var dramaName: UILabel!
}
