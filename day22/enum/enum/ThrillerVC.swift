//
//  ThrillerVC.swift
//  enum
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class ThrillerVC: UIViewController, UICollectionViewDataSource {

    struct ThrillerMovie {
        var poster: String
        var name: String
    }
    
    let thrillers = [
        ThrillerMovie(poster: "shutterIsland.jpg", name: "Shutter Island"),
        ThrillerMovie(poster: "joker.jpg", name: "Joker")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.collectionViewLayout = UICollectionViewFlowLayout()
    }
    
    @IBOutlet var collectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return thrillers.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "thrillerCell", for: indexPath) as! MovieCollectionViewCell
        cell.thrillerPoster.image = UIImage(named: thrillers[indexPath.row].poster)
        cell.thrillerName.text = thrillers[indexPath.row].name
        
        return cell
    }

}

extension ThrillerVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/2.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
}

extension ThrillerVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(thrillers[indexPath.row].name)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
