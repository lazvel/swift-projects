//
//  MovieDetailsVC.swift
//  enum
//
//  Created by lazar.velimirovic on 5/26/21.
//

import UIKit

class MovieDetailsVC: UIViewController{

    var movie: MoviesVC.Movie = MoviesVC.Movie(name: "", year: 0, rated: 0.0, runtime: "", genre: MoviesVC.Genre(rawValue: "") ?? MoviesVC.Genre.action, director: "", writer: "", language: MoviesVC.Language(rawValue: "") ?? MoviesVC.Language.English, country: MoviesVC.Country(rawValue: "") ?? MoviesVC.Country.UnitedStates, production: "", mainActor: "", poster: "")

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBOutlet var poster: UIImageView!
    
    @IBOutlet var name: UILabel!
    @IBOutlet var year: UILabel!
    @IBOutlet var rating: UILabel!
    @IBOutlet var runtime: UILabel!
    @IBOutlet var genre: UILabel!
    @IBOutlet var director: UILabel!
    @IBOutlet var writer: UILabel!
    @IBOutlet var language: UILabel!
    @IBOutlet var country: UILabel!
    @IBOutlet var production: UILabel!
    @IBOutlet var actor: UILabel!
    

    override func viewWillAppear(_ animated: Bool) {
        showDetails()
    }
    
    func showDetails() {
        self.navigationItem.title = movie.name
        
        poster.image = UIImage(named: movie.poster)
        name.text = movie.name
        year.text = String(movie.year)
        rating.text = String(movie.rated)
        runtime.text = movie.runtime
        genre.text = String(movie.genre.rawValue)
        director.text = movie.director
        writer.text = movie.writer
        language.text = String(movie.language.rawValue)
        country.text = String(movie.country.rawValue)
        production.text = movie.production
        actor.text = movie.mainActor
    }

}
