//
//  ViewController.swift
//  enum
//
//  Created by lazar.velimirovic on 5/26/21.
//

import UIKit

class MoviesVC: UIViewController, UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate {
    
    struct Movie {
        var name: String
        var year: Int
        var rated: Double
        var runtime: String
        var genre: Genre
        var director: String
        var writer: String
        var language: Language
        var country: Country
        var production: String
        var mainActor: String
        var poster: String
    }
    
    enum Genre: String {
        case action, thriller, comedy, psychologicalThriller = "psychological thriller", drama, romance
    }
    
    enum Language: String {
        case English, French, Spanish, Serbian, Italian, Japanesse
    }
    
    enum Country: String {
        case Serbia = "Serbia", Italy = "Italy", UnitedStates = "United States", Mexico = "Mexico", Spain, France, Germany, Japan, Russia, Macedonia, Greece
    }
    @IBOutlet var movieList: UITableView!
    
    
//    let best = ["avengers.jpg", "ageOfUltron.jpg", "shutterIsland.jpg", "transporter.jpg", "long-shot.jpg", "notebook.jpg", "taxi4.jpg", "tokyo.jpg", "gone-girl.jpg", "joker.jpg"]
    
    let upcoming = ["f9.jpg", "venom.jpg", "batman.jpg", "hitmansbodyguard.jpg", "escape-room.jpg"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        readFromFile()
        parseModel()
        
        picker.selectRow(2, inComponent: 0, animated: true)
        upcomingImg.image = UIImage(named: "batman.jpg")
    }
    
    @IBOutlet var picker: UIPickerView!
    
    let fileName: String = "movies"
    let fileExtension: String = "csv"
    
    var fileContent: String?
    var movies: [Movie] = []
    
    func readFromFile() {
        let path = Bundle.main.path(forResource: fileName, ofType: fileExtension)
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        if let fileContent = self.fileContent {
            print(fileContent)
            print("----------------------------------------")
        }
    }

    func simpleParseText() {
        if let fileContent = self.fileContent {
            let lineByArray = fileContent.split(separator: "\n")
                print(lineByArray)
            
            for row in lineByArray {
                let lineToArray = row.split(separator: ",")
                for column in lineToArray {
                    print(column)
                }
            }
        }
    }
    
    func parseModel() {
        if let fileContent = self.fileContent {
            let lineByArray = fileContent.split(separator: "\n")
            movies = []
            
            for row in lineByArray {
                let lineToArray = row.split(separator: ",")
                
                let name = String(lineToArray[0])
                let year = Int(lineToArray[1]) ?? 9999
                let rated = Double(lineToArray[2]) ?? 0.0
                let runtime = String(lineToArray[3])
                let genre = String(lineToArray[4])
                let director = String(lineToArray[5])
                let writer = String(lineToArray[6])
                let language = String(lineToArray[7])
                let country = String(lineToArray[8])
                let production = String(lineToArray[9])
                let actor = String(lineToArray[10])
                let poster = String(lineToArray[11])
                
                let newMovie = Movie(name: name, year: year, rated: rated, runtime: runtime, genre: Genre(rawValue: genre)!, director: director, writer: writer, language: Language(rawValue: language)!, country: Country(rawValue: country)!, production: production, mainActor: actor, poster: poster)
                
                movies.append(newMovie)
            }
        }
        
        //print(movies)
    }
   // ==================== TABLE VIEW ====================
    
    let upcomingMovies = ["Fast and Furious 9", "Venom 2", "The Batman", "Hitman's Wife's Bodyguard", "Escape Room 2"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath)
        cell.textLabel?.text = movies[indexPath.row].name
        
        cell.textLabel?.font = UIFont(name: "System", size: 17)
        cell.backgroundColor = .black
        cell.textLabel?.textColor = .white
        cell.layer.borderWidth = 1
        cell.layer.borderColor = UIColor(ciColor: .white).cgColor
        return cell
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return upcomingMovies[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return upcomingMovies.count
    }
    
    @IBOutlet var upcomingImg: UIImageView!
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        picker.reloadComponent(0)
        
        let selectedMovie = pickerView.selectedRow(inComponent: 0)
        let movie = upcoming[selectedMovie]
        
        upcomingImg.image = UIImage(named: movie)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
            let controller = segue.destination as! MovieDetailsVC
            controller.movie = movies[self.movieList.indexPathForSelectedRow!.row]
        }
    }

}

