//
//  MoviesCollectionVC.swift
//  enum
//
//  Created by lazar.velimirovic on 5/26/21.
//

import UIKit

class MoviesCollectionVC: UIViewController, UICollectionViewDataSource {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.collectionViewLayout = UICollectionViewFlowLayout()
    }
    
    struct ActionMovie {
        var poster: String
        var name: String
    }
    
    
    let actions = [
        ActionMovie(poster: "avengers.jpg", name: "The Avengers"),
        ActionMovie(poster: "ageOfUltron.jpg", name: "Avengers: Age of Ultron"),
        ActionMovie(poster: "transporter.jpg", name: "The Transporter"),
        ActionMovie(poster: "tokyo.jpg", name: "F&F Tokyo Drift")
        ]
    
    @IBOutlet var collectionView: UICollectionView!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        actions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "actionCell", for: indexPath) as! MovieCollectionViewCell
        
        cell.actionPoster.image = UIImage(named: actions[indexPath.row].poster)
        cell.movieName.text = actions[indexPath.row].name
        
        return cell
    }

}

extension MoviesCollectionVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = collectionView.bounds.width/2.0
        let yourHeight = yourWidth
        
        return CGSize(width: yourWidth, height: yourHeight)
    }
}

extension MoviesCollectionVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(actions[indexPath.row].name)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.zero
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
