//
//  ViewController.swift
//  playerCatalogue
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class ViewController: UIViewController{
    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeBorderAndCorners()
    }
    @IBOutlet var labela1: UILabel!
    @IBOutlet var labela2: UILabel!
    @IBOutlet var labela3: UILabel!
    @IBOutlet var buttonList: UIButton!
    @IBOutlet var slika: UIImageView!
    
    func makeBorderAndCorners() {
        labela1.layer.borderColor = UIColor.darkGray.cgColor
        labela1.layer.borderWidth = 2.0
        labela1.layer.cornerRadius = 10
        labela1.layer.masksToBounds = true
        
        labela2.layer.borderColor = UIColor.darkGray.cgColor
        labela2.layer.borderWidth = 2.0
        labela2.layer.cornerRadius = 10
        labela2.layer.masksToBounds = true
        
        labela3.layer.borderColor = UIColor.darkGray.cgColor
        labela3.layer.borderWidth = 2.0
        labela3.layer.cornerRadius = 10
        
        buttonList.layer.cornerRadius = 15
    }
    
}

