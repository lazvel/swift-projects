//
//  PlayerInfo.swift
//  playerCatalogue
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class PlayerInfo: UIViewController {
    
    var player: PlayerList.Player = PlayerList.Player(ime: "", prezime: "", starost: 0, visina: 0.0, portret: "", inAction: "")

    override func viewDidLoad() {
        super.viewDidLoad()

        makeCorners()
    }
    @IBOutlet var actionPlayer: UIImageView!
    @IBOutlet var ages: UILabel!
    @IBOutlet var playerHeight: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
        setParameters()
    }
    
    func setParameters() {
        self.navigationItem.title = "\(player.ime) \(player.prezime)"
        actionPlayer.image = UIImage(named: player.inAction)
        ages.text = String(player.starost)
        playerHeight.text = String(player.visina)
        
    }
    
    func makeCorners() {
        actionPlayer.layer.cornerRadius = 20
        ages.layer.cornerRadius = 10
        ages.layer.masksToBounds = true
        playerHeight.layer.cornerRadius = 10
        playerHeight.layer.masksToBounds = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
