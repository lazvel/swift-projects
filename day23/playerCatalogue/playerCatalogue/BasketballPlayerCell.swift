//
//  BasketballPlayerCell.swift
//  playerCatalogue
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class BasketballPlayerCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        makeCorners()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var playerPoster: UIImageView!
    @IBOutlet var name: UILabel!
    @IBOutlet var surname: UILabel!
    @IBOutlet var playerHeight: UILabel!
    @IBOutlet var age: UILabel!
    @IBOutlet var leviStack: UIStackView!
    @IBOutlet var desniStack: UIStackView!
    @IBOutlet var heightSlider: UISlider!
    
    @IBAction func changeSize(_ sender: UISlider) {
        playerHeight.text = String(Int(sender.value))
    }
    
    // MARK: Make Corners and Borders
    func makeCorners() {
        playerPoster.layer.cornerRadius = 0.5 * playerPoster.bounds.size.width
        playerPoster.layer.borderWidth = 1
        playerPoster.layer.borderColor = UIColor(ciColor: .white).cgColor
        playerHeight.layer.cornerRadius = 10
        playerHeight.layer.masksToBounds = true
        
        age.layer.cornerRadius = 10
        age.layer.masksToBounds = true
        
        leviStack.layer.cornerRadius = 10
        leviStack.layer.masksToBounds = true
        
        desniStack.layer.cornerRadius = 10
        desniStack.layer.masksToBounds = true
    }
    
}
