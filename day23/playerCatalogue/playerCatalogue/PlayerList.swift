//
//  PlayerList.swift
//  playerCatalogue
//
//  Created by lazar.velimirovic on 5/27/21.
//

import UIKit

class PlayerList: UIViewController, UITableViewDelegate, UITableViewDataSource {

    override func viewDidLoad() {
        super.viewDidLoad()

        readFromFile()
        parseModel()
    }
    

    // MARK: Struct Definition
    struct Player {
        var ime: String
        var prezime: String
        var starost: Int
        var visina: Float
        var portret: String
        var inAction: String
    }
    
    var players: [Player] = []
    
    // MARK: TEXT Parsing
    var fileContent: String?
    let fileName: String = "igraci"
    let fileExtension: String = "txt"
    
    func readFromFile() {
        let path = Bundle.main.path(forResource: fileName, ofType: fileExtension)
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        if let fileContent = self.fileContent {
            print(fileContent)
            print("----------------------------------------")
        }
    }
    
    func parseModel() {
        if let fileContent = self.fileContent {
            let lineByArray = fileContent.split(separator: "\n")
            players = []
            
            for row in lineByArray {
                let lineToArray = row.split(separator: ",")
                
                let ime = String(lineToArray[0])
                let prezime = String(lineToArray[1])
                let starost = Int(lineToArray[2]) ?? 99
                let visina = Float(lineToArray[3]) ?? 0.0
                let portret = String(lineToArray[4])
                let inAction = String(lineToArray[5])
                
                let newPlayer = Player(ime: ime, prezime: prezime, starost: starost, visina: visina, portret: portret, inAction: inAction)
                
                players.append(newPlayer)
            }
        }

        //print(players)
    }
    
    // MARK: TableView editing
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return players.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! BasketballPlayerCell
        cell.name.text = players[indexPath.row].ime
        cell.surname.text = players[indexPath.row].prezime
        cell.playerPoster.image = UIImage(named: players[indexPath.row].portret)
        cell.age.text = String(players[indexPath.row].starost)
        cell.playerHeight.text = String(players[indexPath.row].visina)
        cell.heightSlider.value = players[indexPath.row].visina
        
        return cell
    }
    // MARK: Segue Prepare
    
    @IBOutlet var playerList: UITableView!
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showInfo" {
            let controller = segue.destination as! PlayerInfo
            controller.player = players[self.playerList.indexPathForSelectedRow!.row]
        }
    }
}
