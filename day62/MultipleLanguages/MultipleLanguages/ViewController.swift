//
//  ViewController.swift
//  MultipleLanguages
//
//  Created by lazar.velimirovic on 7/21/21.
//

import UIKit

class ViewController: UIViewController {
    
    private let myLabel: UILabel = {
        let label = UILabel()
        label.text = "How are you?".localized()
        label.textAlignment = .center
        label.font = .systemFont(ofSize: 32, weight: .semibold)
        return label
    }()
    
    @IBOutlet var signUpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        view.addSubview(myLabel)
//        myLabel.frame = view.bounds
        
        let signUp = NSLocalizedString("SIGN_UP", comment: "This is a sign up button")
        signUpBtn.setTitle(signUp, for: .normal)
    }
}
extension String {
    func localized() -> String {
        return NSLocalizedString(self, tableName: "Localizable", bundle: .main, value: self, comment: self)
    }
}

