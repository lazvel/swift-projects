//
//  User.swift
//  MVPDesignPattern
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation

struct User: Codable {
    let name: String
    let email: String
    let username: String
}
