//
//  Employee.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation

struct Employee {
    var name: String
    var date: String
    var slot: String
}
