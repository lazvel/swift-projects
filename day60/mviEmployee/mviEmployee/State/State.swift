//
//  State.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation

protocol State{
    var employee: [Employee] {get}
    var index: Int {get}
}

struct initialState: State {
    var employee = [
        Employee(name: "Aleksandar", date: "18.06.2021.", slot: "15:00 - 16:00"),
        Employee(name: "Milos", date: "18.06.2021.", slot: "16:00 - 17:00"),
        Employee(name: "Uros", date: "18.06.2021.", slot: "17:00 - 18:00")
    ]
    var index = 0
}

struct ApplicationState: State {
    var employee: [Employee]
    var index: Int
}
