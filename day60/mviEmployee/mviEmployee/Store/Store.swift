//
//  Store.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation
import RxSwift
import RxCocoa

// non terminating sequence, emits most recent values:
// Single Source of Truth

var mainStore: BehaviorRelay<State> = BehaviorRelay<State>(value: initialState())
