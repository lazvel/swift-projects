//
//  Intent.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation
import RxSwift
import RxCocoa

// MARK: MAIN CLASS

class Intent {
    weak var viewController: ViewController? // vc also having the reference of the intent, to avoid cycle reference -> variable will be weak
    var reducer = Reducer()
    private let disposeBag = DisposeBag()
    
    public func onNext() {
        let newState = reducer.getNextState() // generate new state based on user action
        presentNewApplicationState(newState: newState)
    }
    
    private func presentNewApplicationState(newState: ApplicationState) {
        mainStore.accept(newState)
    }
    
    // View/ViewController observing Store for states:
    // bind store in controller
    public func bind(to viewController: ViewController) {
        self.viewController = viewController
        
        mainStore.subscribe { event in
            self.viewController?.update(with: event)
        }.disposed(by: disposeBag)
    }
}
