//
//  ViewController.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    @IBOutlet var employeeNameLabel: UILabel!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var slotLabel: UILabel!
    @IBOutlet var nextButton: UIButton!
    
    private var intent: Intent?
    private let disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        intent = Intent()
        
        intent?.bind(to: self) // making observer
        
        // binding user interaction to Intent
        bindNextButton()
    }
    
    public func update(with employee: Event<State>) {
        if let value = employee.element {
            employeeNameLabel.text = (value.employee[value.index].name)
            dateLabel.text = (value.employee[value.index].date)
            slotLabel.text = (value.employee[value.index].slot)
        }
    }
    
    func bindNextButton() {
        nextButton.rx.tap.bind {
            self.intent?.onNext() // generating the new state
        }.disposed(by: disposeBag)
    }


}

