//
//  Reducer.swift
//  mviEmployee
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation
import RxSwift
import RxCocoa


struct Reducer {
    func getNextState() -> ApplicationState {
        let currentAppState = mainStore.value // global variable mainStore
        
        return ApplicationState(employee: currentAppState.employee, index: (currentAppState.index + 1) % currentAppState.employee.count)
    }
}
