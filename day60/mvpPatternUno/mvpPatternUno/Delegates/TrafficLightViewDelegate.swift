//
//  TrafficLightViewDelegate.swift
//  mvpPatternUno
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation

protocol TrafficLightViewDelegate: NSObjectProtocol {
    func displayTrafficLight(description:(String))
}
