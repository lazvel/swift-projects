//
//  TrafficLight.swift
//  mvpPatternUno
//
//  Created by lazar.velimirovic on 7/19/21.
//

import Foundation

struct TrafficLight {
    let colorName: String
    let description: String
}
