//
//  ViewController.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import UIKit
import SideMenu

class HomeGuestVC: UIViewController, MenuControllerDelegate {
    private var sideMenu: SideMenuNavigationController?
    
    private let calendarController = CalendarEventVC()
    private let couponController = CouponEventVC()
    private let settingsController = SettingsVC()
    
    lazy var menuBarButtonItem = UIBarButtonItem(image: UIImage(named: "menu")?.withRenderingMode(.alwaysOriginal), style: .done, target: self, action: nil)
    
    override func viewWillAppear(_ animated: Bool) {
        if MenuController.isUserActivated {
            let vc = storyboard?.instantiateViewController(identifier: "HomePremiumVC")
            vc?.modalPresentationStyle = .overFullScreen
            navigationController?.present(vc!, animated: true, completion: nil)
            
        }
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
//        let fullUrl = DataProcessing.makeUrl(DataProcessing.baseURL, DataProcessing.publicEventsStr)
//        print(fullUrl)
        
        setupSideMenu()
        setupNavigationController()
    }
    
    func setupSideMenu() {
        let menu = MenuController(with: SideMenuItem.allCases)
        menu.delegate = self
        
        sideMenu = SideMenuNavigationController(rootViewController: menu)
        sideMenu?.leftSide = true
        sideMenu?.setNavigationBarHidden(true, animated: false)
        SideMenuManager.default.leftMenuNavigationController = sideMenu
        SideMenuManager.default.addPanGestureToPresent(toView: view)
        
        //addChildControllers()
    }
    //let sideSetup = SideMenuSetup()
    
//    private func addChildControllers() {
//        addChild(calendarController)
//        addChild(couponController)
//        addChild(settingsController)
//
//        view.addSubview(calendarController.view)
//        view.addSubview(couponController.view)
//        view.addSubview(settingsController.view)
//
//        calendarController.view.frame = view.bounds
//        couponController.view.frame = view.bounds
//        settingsController.view.frame = view.bounds
//
//        calendarController.didMove(toParent: self)
//        couponController.didMove(toParent: self)
//        settingsController.didMove(toParent: self)
//
//        calendarController.view.isHidden = true
//        couponController.view.isHidden = true
//        settingsController.view.isHidden = true
//    }
    
    func setupNavigationController() {
        navigationController?.navigationBar.isHidden = false
        let navigationImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 100, height: 30))
        navigationImageView.image = UIImage(named: "audi_logo")
//        navigationItem.titleView = UIImageView(image: UIImage(named: "audi_logo"))
        navigationItem.leftBarButtonItem?.image = UIImage(named: "menu")
        navigationItem.titleView = navigationImageView
        navigationController?.navigationBar.barTintColor = .white
    }
    
    @IBAction func didTapMenuButton(_ sender: Any) {
        present(sideMenu!, animated: true)
    }
    
    func didSelectMenuItem(named: SideMenuItem) {
        sideMenu?.dismiss(animated: true, completion: nil)
        title = named.rawValue
        
        switch named {
        case .home:
            calendarController.view.isHidden = true
            couponController.view.isHidden = true
            settingsController.view.isHidden = true
        case .calendar:
            // check if there is something to show when its fetched
            let vc = (storyboard?.instantiateViewController(identifier: "CalendarVC"))!
//            let nc = UINavigationController(rootViewController: vc)
//            vc.modalPresentationStyle = .fullScreen
            //self.present(nc, animated: false, completion: nil)
            
            present(vc, animated: false, completion: nil)
//            calendarController.view.isHidden = false
//            couponController.view.isHidden = true
//            settingsController.view.isHidden = true
        case .coupon:
            let alertClass = CustomAlert()
            
            alertClass.showAlert()
            present(alertClass.alert, animated: true, completion: nil)
            calendarController.view.isHidden = true
            couponController.view.isHidden = true
            settingsController.view.isHidden = true
        case .settings:
            let vc = (storyboard?.instantiateViewController(identifier: "SettingsVC"))! // SettingsVC
            present(vc, animated: false, completion: nil)
            calendarController.view.isHidden = true
            couponController.view.isHidden = true
            settingsController.view.isHidden = false
        }
    }
    
    @IBAction func insertCoupon(_ sender: Any) {
        let alertClass = CustomAlert()
        
        alertClass.showAlert()
        present(alertClass.alert, animated: true, completion: nil)
    }
    
}

