//
//  CalendarEventVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/5/21.
//

import UIKit

class CalendarEventVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //print(TestData.premiumEvents ?? "nista")
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(TestData.premiumEvents!.count)
        return TestData.publicEvents?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventiCell", for: indexPath) as! EventiTableCell
        if let urlString = TestData.publicEvents?[indexPath.row].backgroundImage?.href as? String, let url = URL(string: urlString){
                    URLSession.shared.dataTask(with: url){ (data,response,error) in
                        if let data = data{
                        DispatchQueue.main.async {
                                cell.bannerPhoto.image = UIImage(data: data)
                            }
                        }
                    }.resume()

                }
        cell.descriptionLabel.text = TestData.publicEvents![indexPath.row].description.value
        cell.title1Label.text = TestData.publicEvents![indexPath.row].title
        return cell
        
        
    }
    
//    func updateUI() {
//        let url: String = ""
////        for data in TestData.publicEvents{
////
////        }
//        let task = URLSession.shared.dataTask(with: TestData.publicEvents![indexPath.row].backgroundImage?.href, completionHandler: { (data, response, error) in
//                guard let data = data,
//                      let image = UIImage(data: data) else {return}
//                DispatchQueue.main.async {
//                    self.photo.image = image
//                }
//            })
//        
//        task.resume()
//    }
}
