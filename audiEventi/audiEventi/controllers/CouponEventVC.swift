//
//  CouponEventVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/5/21.
//

import UIKit

class CouponEventVC: UIViewController {

    static let alert = UIAlertController(title: "Codice", message: "Inerisci qui sotto il tuo Coupon Evento Audi e premi il pulsante procedi per acedere a tutti i contenuti relative all'evento a cui sei iscritto.", preferredStyle: .alert)
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //showAlert()
    }
    let codici = "555333"
    static func showAlert() {
        CouponEventVC.alert.addTextField { field in
            field.placeholder = "Codi inserici"
            field.returnKeyType = .next
            field.keyboardType = .default
        }
//        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
        CouponEventVC.alert.addAction(UIAlertAction(title: "Procedi", style: .default, handler: { _ in
            guard let fields = alert.textFields, fields.count == 1 else {
                return
            }
            let codeField = fields[0]
            guard let code = codeField.text, !code.isEmpty else {
                print("Invalid entries")
                return
            }
//                self.present(vc, animated: true, completion: nil)
//            }
            print("Codice: \(code)")
        }))
    }

}
