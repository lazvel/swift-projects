//
//  InfoAndContactsVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/9/21.
//

import UIKit

class InfoAndContactsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TestData.contacts?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell", for: indexPath) as! InfoContactsCell
        cell.contactNameLabel.text = TestData.contacts?[indexPath.row].title
        cell.organizationNameLabel.text = TestData.contacts?[indexPath.row].contactTitle.contactName
        cell.locationLabel.text = TestData.contacts?[indexPath.row].contactTitle.location
        cell.phoneLabel.text = TestData.contacts?[indexPath.row].contactTitle.phone
        cell.emailLabel.text = TestData.contacts?[indexPath.row].contactTitle.mail
        
        return cell
    }
}
