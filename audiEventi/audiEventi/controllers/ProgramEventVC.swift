//
//  ProgramEventVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/12/21.
//

import UIKit

class ProgramEventVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var twoDimensionalArr = [
        TestData.ExpandablePrograms(isExpanded: false, dates: "Venerdi 26 Febbraio", times: ["7:30", "7:30 - 9:30", "9:30 - 11:00", "11:30 - 15:00", "15:30 - 16:30", "Dalle 17:30",], programs: ["Benvenuto e brief tecnico con Herve Barmasse", "SkiAlp", "Colazione in rifugio e inspirational talk", "Tempo libero", "Audi Driving Experience", "Tempo libero e pernottamento"]),
        TestData.ExpandablePrograms(isExpanded: true, dates: "Sabatp 27 Febbraio", times: ["7:30", "7:30 - 9:30", "9:30 - 11:00", "11:30 - 15:00", "15:30 - 16:30", "Dalle 17:30",], programs: ["Benvenuto e brief tecnico con Herve Barmasse", "SkiAlp", "Colazione in rifugio e inspirational talk", "Tempo libero", "Audi Driving Experience", "Tempo libero e pernottamento"]),
        TestData.ExpandablePrograms(isExpanded: false, dates: "Domenica 28 Febbraio", times: ["7:30", "7:30 - 9:30", "9:30 - 11:00", "11:30 - 15:00", "15:30 - 16:30", "Dalle 17:30",], programs: ["Benvenuto e brief tecnico con Herve Barmasse", "SkiAlp", "Colazione in rifugio e inspirational talk", "Tempo libero", "Audi Driving Experience", "Tempo libero e pernottamento"])
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

//        tableView.register(<#T##cellClass: AnyClass?##AnyClass?#>, forCellReuseIdentifier: <#T##String#>)
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let button = UIButton(type: .system)
        button.setTitle(twoDimensionalArr[section].dates, for: .normal)
        button.setTitleColor(.black, for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        let isExpanded = twoDimensionalArr[section].isExpanded
        
        button.setImage(isExpanded ? UIImage(systemName: "chevron.up") : UIImage(systemName: "chevron.down"), for: .normal)
        
        button.tintColor = .darkGray
        button.semanticContentAttribute = .forceRightToLeft
        //button.alig
        button.imageEdgeInsets = UIEdgeInsets(top: 5, left: 120, bottom: 5, right: 0)
        let border = UIView(frame: CGRect(x: 0, y: 40,width: self.view.bounds.width - 20, height: 1))
        border.backgroundColor = .darkGray
        
        button.backgroundColor = .white
        button.addSubview(border)
        
        button.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        
        button.tag = section
        
        return button
    }
    
    @objc func handleExpandClose(button: UIButton) {
        print("Trying to expand and close section")
        let section = button.tag
        
        
        // try to close the section first by deletig the rows
        var indexPaths = [IndexPath]()
        for row in twoDimensionalArr[section].times.indices {
            print(0, row)
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        let isExpanded = twoDimensionalArr[section].isExpanded
        twoDimensionalArr[section].isExpanded = !isExpanded
        
        button.semanticContentAttribute = .forceRightToLeft
        button.tintColor = .black
        button.setImage(isExpanded ? UIImage(systemName: "chevron.down") : UIImage(systemName: "chevron.up"), for: .normal)
        //button.layer.masksToBounds = true
        
        if isExpanded {
            tableView.deleteRows(at: indexPaths, with: .fade)
        } else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    
    @IBOutlet var tableView: UITableView!
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return twoDimensionalArr.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if !twoDimensionalArr[section].isExpanded {
            return 0
        }
        return twoDimensionalArr[section].times.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "programCell", for: indexPath) as! ProgramTableCell
        
        cell.timeLabel.text = twoDimensionalArr[indexPath.section].times[indexPath.row]
        cell.programNameLabel.text = twoDimensionalArr[indexPath.section].programs[indexPath.row]
        return cell
    }
}
