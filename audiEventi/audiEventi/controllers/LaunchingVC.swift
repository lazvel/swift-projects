//
//  LaunchingVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/8/21.
//

import UIKit

class LaunchingVC: UIViewController {

    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationController?.navigationBar.isHidden = true
        activityIndicator.startAnimating()
        
        DataProcessing.fetchLangs(url: DataProcessing.langsURL) { (result) in
            guard let result = result else { return }
            //let data = result
            
            DataProcessing.sideMenuGuest?.append(result.SIDE_MENU_GUEST_HOME_PAGE)
            DataProcessing.sideMenuGuest?.append(result.SIDE_MENU_GUEST_EVENT_LIST)
            DataProcessing.sideMenuGuest?.append(result.SIDE_MENU_GUEST_COUPON_LOGIN)
            DataProcessing.sideMenuGuest?.append(result.SIDE_MENU_GUEST_SETTINGS)
            
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_GUEST_HOME_PAGE)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_EVENT_DETAIL)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_FOOD_EXPERIENCE)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_LOCATION)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_ADE)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_SURVEY)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_GUEST_EVENT_LIST)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_GUEST_COUPON_LOGIN)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_PREMIUM_EVENT_INFO)
            DataProcessing.sideMenuPremium?.append(result.SIDE_MENU_GUEST_SETTINGS)
            
            DataProcessing.langsList = result
            
        }
        TestData.populateData()
        
        DispatchQueue.main.async {
            sleep(2)
            self.performSegue(withIdentifier: "homeGuest", sender: self)
        }
    }
    
}
