//
//  PlacesAndTerritoriesVC.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/14/21.
//

import UIKit

class PlacesAndTerritoriesVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var timer = Timer()
    var counter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        pageView.numberOfPages = TestData.imageArr.count
        pageView.currentPage = 0
        
        DispatchQueue.main.async {
            self.timer = Timer.scheduledTimer(timeInterval: 4.0, target: self, selector: #selector(self.changeImage), userInfo: nil, repeats: true)
        }
        setImages()
        
    }
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var sliderCollectionView: UICollectionView!
    @IBOutlet var pageView: UIPageControl!
    
    func setImages() {
        for i in 0...TestData.imageArr.count-1 {
            self.pageView.setIndicatorImage(UIImage(systemName: "rectangle.fill")!, forPage: i)
        }
    }
    
    @objc func changeImage() {
        if counter < TestData.imageArr.count {
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: true)
            pageView.currentPage = counter
            counter += 1
        } else {
            counter = 0
            let index = IndexPath.init(item: counter, section: 0)
            self.sliderCollectionView.scrollToItem(at: index, at: .centeredHorizontally, animated: false)
            pageView.currentPage = counter
            counter = 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TestData.imageArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "sliderCell", for: indexPath) as! SliderCell
        cell.imageSlider.image = TestData.imageArr[indexPath.row]
        let ab = UIPageControl()
        ab.currentPage = indexPath.row
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        counter = indexPath.row
        pageView.reloadInputViews()
    }
}

extension PlacesAndTerritoriesVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = sliderCollectionView.frame.size
        return CGSize(width: size.width, height: size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
