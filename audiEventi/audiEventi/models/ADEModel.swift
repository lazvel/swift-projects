//
//  ADEModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct AudiDriveExperience: Codable {
    var data: [AudiDriveExperienceData]
    var result: String
    var resultCode: String
    var resultMessage: String
    
//    enum CodingKeys: String, CodingKey {
//        case data = ""
//        case result = ""
//        case resultCode = ""
//        case resultMessage = ""
//    }
}

struct AudiDriveExperienceData: Codable {
    var id: String
    var description: String
    var subtitleAde: String
    var titleAde: String
    var sliderImageAde: [BackgroundImage]
    var imageAde: BackgroundImage
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case description = ""
//        case subtitleAde = ""
//        case titleAde = ""
//        case sliderImageAde = ""
//        case imageAde = ""
//    }
}
