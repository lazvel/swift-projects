//
//  ContactsAndInfoModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct ContactsAndInfo: Codable {
    var data: [ContactsAndInfoData]
    var result: String
    var resultCode: String
    var resultMessage: String
    
//    enum CodingKeys: String, CodingKey {
//        case data = ""
//        case result = ""
//        case resultCode = ""
//        case resultMessage = ""
//    }
}

struct ContactsAndInfoData: Codable {
    var id: String
    var title: String
    var contactTitle: ContactTitle
    //var freeText: FreeText 
    //var contactImage: BackgroundImage
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case title = ""
//        
//        
//        case contactImage = ""
//    }
}
struct ContactTitle: Codable {
    var contactName: String
    var location: String
    var phone: String
    var mail: String
}
