//
//  FoodExperienceModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct FoodExperience: Codable {
    var data: [FoodExperienceData] // is it array od FoodData ?
    var result: String
    var resultCode: String
    var resultMessage: String
    
//    enum CodingKeys: String, CodingKey {
//        case data = ""
//        case result = ""
//        case resultCode = ""
//        case resultMessage = ""
//    }
}

struct FoodExperienceData: Codable {
    var id: String
    var title: String
    var header: String
    var foodSubtitle: FoodSubtitle
    var foodImage: BackgroundImage // same from public Event
    var programExperience: [ProgramExperience]
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case title = ""
//        case header = ""
//        case foodSubtitle = ""
//        case foodImage = ""
//        case programExperience = ""
//    }
}

struct FoodSubtitle: Codable {
    var value: String
    var format: String
    var processed: String
    
//    enum CodingKeys: String, CodingKey {
//        case value = ""
//        case format = ""
//        case processed = ""
//    }
}

struct ProgramExperience: Codable {
    var day: String
    var start: String
    var type: String
    var activity: String
    var site: String
    var description: String // formated html
    var food: String // formated html
    var alergens: String // formated html
    
//    enum CodingKeys: String, CodingKey {
//        case day = ""
//        case start = ""
//        case type = ""
//        case activity = ""
//        case site = ""
//        case description = ""
//        case food = ""
//        case alergens = ""
//    }
}

