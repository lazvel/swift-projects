//
//  Model.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct PublicEvent: Codable {
    var id: String
    var title: String
    var description: PublicDescription
    var dataEvent: String
    var linkMyAudi: LinkMyAudi
    var priority: Int
    var status: String
    var backgroundImage: BackgroundImage?
    
//    enum CodingKeys: String, CodingKey {
//        case id =  ""
//        case title =  ""
//        case description =  ""
//        case dataEvent =  ""
//        case linkMyAudi =  ""
//        case priority =  ""
//        case status =  ""
//        case backgroundImage =  ""
//    }
    
}

struct PublicDescription: Codable {
    var format: String
    var value: String
    
//    enum CodingKeys: String, CodingKey {
//        case format = ""
//        case value = ""
//    }
}

struct LinkMyAudi: Codable {
    var uri: String
    var title: String
    var options: [Option]
    
//    enum CodingKeys: String, CodingKey {
//        case uri = ""
//        case title = ""
//        case options = ""
//    }
}

// typical empty ??
struct Option: Codable {
    
}

struct BackgroundImage: Codable {
    var id: String
    var href: String
    var meta: Meta
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case href = ""
//        case meta = ""
//    }
}

struct Meta: Codable {
    var alt: String
    var title: String
    var width: Int
    var height: Int
    
//    enum CodingKeys: String, CodingKey {
//        case alt = ""
//        case title = ""
//        case width = ""
//        case height = ""
//    }
}
