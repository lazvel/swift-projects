//
//  CouponModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/2/21.
//

import Foundation

struct CuponVerification: Codable, ResultProtocol, CouponProtocol {
    var coupon: String
    var eventId: String
    var status: String
    var valid: String
    var eventStatus: String
    var result: String
    var resultCode: String
    var resultMessage: String
}

struct CouponValidation: Codable, CouponProtocol {
    var coupon: String
    var eventId: String
    var status: String
    var valid: String
    var action: String
}



