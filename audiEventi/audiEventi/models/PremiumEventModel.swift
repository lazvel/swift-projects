//
//  PremiumEventModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct PremiumEvent: Codable {
    var data: [PremiumEventData]
    var result: String
    var resultCode: String
    var resultMessage: String
    
    // ADD ENUM KEYS FOR ALL STRUCTURES
}

struct PremiumEventData: Codable {
    var id: String
    var title: String
    var description: PremiumDescription
    var headerPremium: String
    var linkMyAudiPremium: LinkMyAudi
    var noteProgram: NoteProgram
    // WHAT IS ARRAY OF OBJECTS DAY ? containing Activities
    var programDetails: [ProgramDetail]
    var subtitle: String
    var image: BackgroundImage
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case title = ""
//        case description = ""
//        case headerPremium = ""
//        case linkMyAudiPremium = ""
//        case noteProgram = ""
//        case programDetails = ""
//        case subtitle = ""
//        case image = ""
//    }
}

struct PremiumDescription: Codable {
    var value: String
    var format: String
    var processed: String
    
//    enum CodingKeys: String, CodingKey {
//        case value = ""
//        case format = ""
//        case processed = ""
//    }
}

struct NoteProgram: Codable {
    var value: String
    var format: String
    var processed: String // html tags ??
    
//    enum CodingKeys: String, CodingKey {
//        case value = ""
//        case format = ""
//        case processed = ""
//    }
}

// what is in detail, is Day Object with Activities array
struct ProgramDetail: Codable {
    var day: String
    var activities: [Activity]
    
//    enum CodingKeys: String, CodingKey {
//        case day = ""
//        case activities = ""
//    }
}

struct Activity: Codable {
    var start : String
    var end : String
    var activity: String
    
//    enum CodingKeys: String, CodingKey {
//        case start = ""
//        case end = ""
//        case activity = ""
//    }
}


