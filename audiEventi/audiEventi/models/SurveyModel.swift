//
//  SurveyModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct SurveyResponse: Codable {
    var questions: [Question]
    var status: String // enum ?
    var result: String // enum ?
    var resultCode: String // enum ?
    var resultMessage: String // enum ?
    
//    enum CodingKeys: String, CodingKey {
//        case questions = ""
//        case status = ""
//        case result = ""
//        case resultCode = ""
//        case resultMessage = ""
//    }
}

struct Question: Codable {
    var webformId: String
    var title: String
    var questionId: String
    var required: String
    var requiredError: String
    var webformMultiple: String
    var type: String // ENUMS type of demand ?? radio buttons, text area, button
    
    // Is it needed ?
//    var result: String // enum ?
//    var resultCode: String // enum ?
//    var resultMessage: String // enum ?
    
//    enum CodingKeys: String, CodingKey {
//        case webformId = ""
//        case title = ""
//        case questionId = ""
//        case required = ""
//        case requiredError = ""
//        case webformMultiple = ""
//        case type = ""
//    }
}

struct SurveyPost: Codable {
    var options: [String] // from enums
    var questions: String
    var other: String
    var answers: String
    
//    enum CodingKeys: String, CodingKey {
//        case options = ""
//        case questions = ""
//        case other = ""
//        case answers = ""
//    }
}
