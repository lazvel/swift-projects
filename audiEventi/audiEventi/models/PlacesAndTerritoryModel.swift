//
//  PlacesAndTerritoryModel.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/1/21.
//

import Foundation

struct PlacesAndTerritoryResponse: Codable {
    var data: [PlacesAndTerritoryData]
    var result: String
    var resultCode: String
    var resultMessage: String
    
//    enum CodingKeys: String, CodingKey {
//        case data = ""
//        case result = ""
//        case resultCode = ""
//        case resultMessage = ""
//    }
}

struct PlacesAndTerritoryData: Codable {
    var id: String
    var title: String
    var description: String
    var placeSubtitle: String
    var placeTitle: String
    var placesSlider: [BackgroundImage]
    var imagePlace: BackgroundImage
    
//    enum CodingKeys: String, CodingKey {
//        case id = ""
//        case title = ""
//        case description = ""
//        case placeSubtitle = ""
//        case placeTitle = ""
//        case placesSlider = ""
//        case imagePlace = ""
//    }
}
