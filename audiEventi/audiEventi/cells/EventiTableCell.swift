//
//  EventiTableCell.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/6/21.
//

import UIKit

class EventiTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var bannerPhoto: UIImageView!
    @IBOutlet var title1Label: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    
}
