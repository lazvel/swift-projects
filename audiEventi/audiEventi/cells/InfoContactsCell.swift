//
//  InfoContactsCell.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/12/21.
//

import UIKit

class InfoContactsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var contactNameLabel: UILabel!
    @IBOutlet var organizationNameLabel: UILabel!
    @IBOutlet var locationLabel: UILabel!
    @IBOutlet var phoneLabel: UILabel!
    @IBOutlet var emailLabel: UILabel!
    

}
