//
//  TerritorySliderCell.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/14/21.
//

import UIKit

class SliderCell: UICollectionViewCell {
    
    @IBOutlet var imageSlider: UIImageView!
    @IBOutlet var imageSliderAde: UIImageView!
}
