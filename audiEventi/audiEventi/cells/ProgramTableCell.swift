//
//  ProgramTableCell.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/13/21.
//

import UIKit

class ProgramTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var programNameLabel: UILabel!
    
}
