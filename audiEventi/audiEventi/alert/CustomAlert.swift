//
//  CustomAlert.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/7/21.
//

import Foundation
import UIKit

class CustomAlert: UIViewController {
    let alert = UIAlertController(title: "Codice", message: "Inerisci qui sotto il tuo Coupon Evento Audi e premi il pulsante procedi per acedere a tutti i contenuti relative all'evento a cui sei iscritto.", preferredStyle: .alert)
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        present(CustomAlert.alert, animated: true, completion: nil)
//        showAlert()
//    }
    static let codici = "555333"
    func showAlert() {
        alert.addTextField { field in
            field.placeholder = "Codi inserici"
            field.returnKeyType = .next
            field.keyboardType = .default
        }
//        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: {_ in 
//            return
//        }))
        
        alert.addAction(UIAlertAction(title: "Procedi", style: .default, handler: { _ in
            guard let fields = self.alert.textFields, fields.count == 1 else {
                return
            }
            let codeField = fields[0]
            guard let code = codeField.text, !code.isEmpty else {
                print("Invalid entries")
                return
            }
            print("Codice: \(code)")
        }))
//        let vc = (storyboard?.instantiateViewController(identifier: "PremiumVC"))!
    }
}
