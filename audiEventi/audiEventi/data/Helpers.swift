//
//  Helpers.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/7/21.
//

import Foundation

extension URL {
    func withHTTPS() -> URL? {
        var components = URLComponents(url: self, resolvingAgainstBaseURL: true)
        components?.scheme = "https"
        
        return components?.url
    }
}
