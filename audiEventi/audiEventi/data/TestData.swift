//
//  TestData.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/7/21.
//

import Foundation
import UIKit

class TestData {
    struct ExpandablePrograms {
        var isExpanded: Bool
        let dates: String
        let times: [String]
        let programs: [String]
    }
    
    static let publicEventJson = """
[{
    "id": "1",
    "title": "Audi Talks Performance: ieri, oggi, e domani.",
    "description": {
        "format": "",
        "value": "Otto puntate con ospiti d'eccezione per condividare visioni all'avangurdia e discutere di una nouva, straordinaria idea"

    },
    "dataEvent": "02.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 4,
    "status": "OK",
    "backgroundImage": {
        "id": "1",
        "href": "https://cdn-rs.audi.at/media/ThreeColTeaser_TextImage_Image_Component/12754-paragraphs-75119-51281-image/dh-925-bc764f/ae4a3c6b/1622795799/1920x1080-exterior-aq7-191010-oe.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}, {
    "id": "2",
    "title": "Cortina e-portrait",
    "description": {
        "format": "",
        "value": "Dal 2017 Audi - per tutilare Cortina come patrimonio si e fatta promotrice con il Comune di Ampezzo di un progetto di Corporate..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 1,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://audimediacenter-a.akamaihd.net/system/production/media/100507/images/34f86323badc5d81b917632c2d53060d83cc0383/A212282_x500.jpg?1618561779",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "3",
    "title": "Mountaineering Workshop con Herve ...",
    "description": {
        "format": "",
        "value": "Un evento dedicato alla montagna e al guisto approccio per viverla con rispetto e inteligenza, dalla attivita sportive come lo sci ..."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []

    },
    "priority": 2,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://cdn-rs.audi.at/media/Theme_Menu_Model_Dropdown_Item_Image_Component/root-rs-model-modelMenu-editableItems-13719-dropdown-314510-image/dh-478-a0e9a6/9134d247/1622795873/1920x1920-audi-q5-sportback-my2021-1342-big-oe.jpg",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
},{
    "id": "4",
    "title": "Performance Workshop con Kristian Ghedina",
    "description": {
        "format": "",
        "value": "I pluricampioni Kristian Ghedina e Peter Fill, insieme agli chef stellati Costardi Bros, ti guideranno in un 'esperienza totale, tra sport."

    },
    "dataEvent": "03.09.2021",
    "linkMyAudi": {
        "uri": "www.audi.de",
        "title": "Audi",
        "options": []
    },
    "priority": 3,
    "status": "OK",
    "backgroundImage": {
        "id": "2",
        "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
        "meta": {
            "alt": "loading..",
            "title": "Meta",
            "width": 200,
            "height": 300
        }
    }
}

]
"""
static let premiumEventJson = """
    {
        "data": [
            {
             "id": "1",
             "title": "Uomini fiesta del Audi",
             "description": {
                 "value": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
                "format": "nn",
                "processed": "Y"
             },
             "headerPremium": "Bene bene",
             "linkMyAudiPremium": {
                "uri": "www.audi.de",
                "title": "Audi",
                "options": []
             },
             "noteProgram": {
                "value": "Culinigo umelana",
                "format": "Som",
                "processed": "Uno"
             },
             "programDetails": [{
                    "day": "1",
                    "activities": [{
                           "start" : "16:30",
                           "end" : "20:30",
                           "activity": "Survey disponibile"
                        }]
             }],
             "subtitle": "Madona di Camiglio",
             "image": {
                "id": "1",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
             }
        },
            {
             "id": "2",
             "title": "Lorem ipsum dolor sit",
             "description": {
                 "value": "voluptate velit esse cillum dolore eu fugiat nulla pariatur. eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ",
                "format": "UN",
                "processed": "Y"
             },
             "headerPremium": "Someth someth",
             "linkMyAudiPremium": {
                "uri": "www.audi.de",
                "title": "Audi",
                "options": []
             },
             "noteProgram": {
                "value": "Culinigo umelana",
                "format": "Som",
                "processed": "Uno"
             },
             "programDetails": [{
                    "day": "1",
                    "activities": [{
                           "start" : "12:00",
                           "end" : "15:30",
                           "activity": "Con Herve"
                        }]
             }],
             "subtitle": "Mountaneering con Herve Barnas",
             "image": {
                "id": "1",
                "href": "https://s1.cdn.autoevolution.com/images/news/gallery/audi-a3-lamborghini-edition-looks-like-doable-german-italian-craftsmanship_8.jpg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
             }
        },
            {
             "id": "3",
             "title": "Ut enim ad minim",
             "description": {
                 "value": "voluptate velit esse cillum dolore eu fugiat nulla pariatur. eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in ",
                "format": "RE",
                "processed": "Y"
             },
             "headerPremium": "Ut enim ad minim",
             "linkMyAudiPremium": {
                "uri": "www.audi.de",
                "title": "Audi",
                "options": []
             },
             "noteProgram": {
                "value": "Programa tres",
                "format": "Ul",
                "processed": "Y"
             },
             "programDetails": [{
                    "day": "1",
                    "activities": [{
                           "start" : "10:00",
                           "end" : "12:00",
                           "activity": "Nota ita"
                        }]
             }],
             "subtitle": "Dirt rally",
             "image": {
                "id": "1",
                "href": "https://cdn-rs.audi.at/media/ThreeColTeaser_TextImage_Image_Component/12754-paragraphs-75119-51281-image/dh-925-bc764f/ae4a3c6b/1622795799/1920x1080-exterior-aq7-191010-oe.jpg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
             }
        }
        ],
        "result": "200",
        "resultCode": "200",
        "resultMessage": "OK"
    }
"""
    
static let contactsAndInfoJson = """
{
    "data": [{
            "id": "1",
            "title": "CONTATTI",
            "contactTitle": {
                "contactName": "Segretaria Organizzativa",
                "location": "Napoli",
                "phone": "T. +39 0236705020",
                "mail": "booking@mkeventi.com"
            }
        },
        {
            "id": "2",
            "title": "Boutique Hotel Diana 4S",
            "contactTitle": {
                "contactName": "Via Cima Tosa, 52",
                "location": "38086, Madonna di Campiglio (TN)",
                "phone": "T. +39 0465 441011",
                "mail": "info@hoteldiana.net"
            }
        },
        {
            "id": "3",
            "title": "Alpen Suite Hotel",
            "contactTitle": {
                "contactName": "viale Dolomiti di Brenta 84",
                "location": "38086, Madonna di Campiglio (TN)",
                "phone": "T. +39 0465 440100 F +39 0465 440409",
                "mail": "info@alpensuitehotel.net"
            }
        }
    ],
    "result": "200",
    "resultCode": "200",
    "resultMessage": "OK"
}
"""
    
static let placesAndTerritoryJson = """
{
    "data": [{
        "id": "1",
        "title": "Uno Due Tre Divergenti",
        "description": "Suspendisse auctor at nulla a ultrices. Suspendisse accumsan diam vitae eleifend blandit. Praesent a augue faucibus, feugiat diam ac, semper elit. Suspendisse et semper quam, quis euismod tortor. Integer congue ut risus id interdum. Fusce dapibus metus nec augue porta, blandit scelerisque sem sagittis. Maecenas interdum turpis vitae tortor euismod efficitur vitae sed massa.",
        "placeSubtitle": "Cognos",
        "placeTitle": "Nepal",
        "placesSlider": [{
            "id": "1",
            "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
            "meta": {
                "alt": "loading..",
                "title": "Meta",
                "width": 200,
                "height": 300
            }
        }],
        "imagePlace": {
            "id": "1",
            "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
            "meta": {
                "alt": "loading..",
                "title": "Meta",
                "width": 200,
                "height": 300
            }
        }
    }],
    "result": "200",
    "resultCode": "200",
    "resultMessage": "OK"
}
"""

static let foodExperienceJson = """
{
    "data": [{
        "id": "1",
        "title": "TITLE1",
        "header": "Jjaiojai najo",
        "foodSubtitle": {
            "value": "String",
            "format": "",
            "processed": ""
        },
        "foodImage": {
            "id": "1",
            "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
            "meta": {
                "alt": "loading..",
                "title": "Meta",
                "width": 200,
                "height": 300
            }
        },
        "programExperience": [
            {
                "day": "11.01.2021.",
                "start": "08:00",
                "type": "",
                "activity": "brekfast",
                "site": "",
                "description": "",
                "food": "",
                "alergens": ""
            },
            {
                "day": "21.02.2021.",
                "start": "",
                "type": "",
                "activity": "",
                "site": "",
                "description": "",
                "food": "",
                "alergens": ""
            }
        ]
    }],
    "result": "200",
    "resultCode": "200",
    "resultMessage": "OK"
}
"""
    
static let surveyResponseJson = """
{
    "questions": [{
        "webformId": "1",
        "title": "Randomago confirmato",
        "questionId": "1",
        "required": "Required",
        "requiredError": "Required Error",
        "webformMultiple": "Web Form",
        "type": "Type"
    }],
    "status": "Accepted",
    "result": "200",
    "resultCode": "200",
    "resultMessage": "OK"
}
"""

static let adeJson = """
{
    "data": [{
        "id": "1",
        "description": "È universalmente riconosciuto che un lettore che osserva il layout di una pagina viene distratto dal contenuto testuale se questo è leggibile. Lo scopo dell’utilizzo del Lorem Ipsum è che offre una normale distribuzione delle lettere (al contrario di quanto avviene se si utilizzano brevi frasi ripetute, ad esempio “testo qui”), apparendo come un normale blocco di testo leggibile. Molti software di impaginazione e di web design utilizzano Lorem",
        "subtitleAde": "Al contrario di quanto si pensi, Lorem Ipsum ",
        "titleAde": "Audi driving Experience",
        "sliderImageAde": [{
                "id": "2",
                "href": "https://autoblog.rs/gallery/108/201217-audi%20a3%2033.jpg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            },
            {
                "id": "2",
                "href": "https://c4.wallpaperflare.com/wallpaper/675/310/349/audi-rs6-audi-rs6-avant-quattro-wallpaper-preview.jpg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }, {
                "id": "2",
                "href": "https://www.autocar.co.uk/sites/autocar.co.uk/files/styles/gallery_slide/public/images/car-reviews/first-drives/legacy/18_audi_a4_e-tron_rs_render_2021.jpg?itok=vouYnOJ0",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }, {
                "id": "2",
                "href": "https://audiclubserbia.com/club/wp-content/uploads/2021/02/1-audi-e-tron-s-sportback-2021-uk-first-drive-review-hero-front-450x290@2x.jpg",
                "meta": {
                    "alt": "loading..",
                    "title": "Meta",
                    "width": 200,
                    "height": 300
                }
            }
        ],
        "imageAde": {
            "id": "2",
            "href": "https://topspeed.rs/gallery/thumb/foto-audi-1/page/1/marker/30868/photo/167060.jpeg",
            "meta": {
                "alt": "loading..",
                "title": "Meta",
                "width": 200,
                "height": 300
            }
        }
    }],
    "result": "200",
    "resultCode": "200",
    "resultMessage": "OK"
}
"""
    
    static var publicEvents: [PublicEvent]?
    static var premiumEvents: [PremiumEventData]?
    static var placesAndTerritories: [PlacesAndTerritoryData]?
    static var foodExperiences: [FoodExperienceData]?
    static var contacts: [ContactsAndInfoData]?
    static var questions: [Question]?
    static var ades: [AudiDriveExperienceData]?
    
    static let decoder = JSONDecoder()
    
    static let jsonStr = publicEventJson.data(using: .utf8)
    static let jsonPremium = premiumEventJson.data(using: .utf8)
    static let jsonPlaces = placesAndTerritoryJson.data(using: .utf8)
    static let jsonFoodExperience = foodExperienceJson.data(using: .utf8)
    static let jsonContactAndInfo = contactsAndInfoJson.data(using: .utf8)
    static let jsonSurvey = surveyResponseJson.data(using: .utf8)
    static let jsonADE = adeJson.data(using: .utf8)
    
    static var publicEvent: PublicEvent?
    
    static func populateData() {
        publicEvents = []
        premiumEvents = []
        if let data = jsonStr, let result = try? decoder.decode([PublicEvent].self, from: data) {
            publicEvents = result
            //publicEvent = result[0]
            //print(publicEvents)
        }
        if let data2 = jsonPremium, let result = try? decoder.decode(PremiumEvent.self, from: data2) {
            //print("Usao u let 2")
            premiumEvents = result.data
            //print(premiumEvents)
        }
        if let data3 = jsonPlaces, let result = try? decoder.decode(PlacesAndTerritoryResponse.self, from: data3) {
            //print("Usao u let 3")
            placesAndTerritories = result.data
            //print(placesAndTerritories)
        }
        if let data4 = jsonFoodExperience, let result = try? decoder.decode(FoodExperience.self, from: data4) {
            //print("Usao u let 4")
            //print(result)
            foodExperiences = result.data
            //print("Food\(String(describing: foodExperiences))")
        }
        if let data5 = jsonContactAndInfo, let result = try? decoder.decode(ContactsAndInfo.self, from: data5) {
            contacts = result.data
            //print(contacts)
        }
        if let data6 = jsonSurvey, let result = try? decoder.decode(SurveyResponse.self, from: data6) {
            //print("Usao u Survey handling")
            questions = result.questions
            //print(questions)
        }
        
        if let data7 = jsonADE, let result = try? decoder.decode(AudiDriveExperience.self, from: data7) {
            //print("Usao u ADE handling")
            ades = result.data
            //print(ades)
        }
    }
    
    static var imageArr = [
        UIImage(named: "img_survey"),
        UIImage(named: "sondaggio"),
        UIImage(named: "drivingExp"),
        UIImage(named: "infoContatti")
    ]
    
    static var imageAdeArr = [
        UIImage(named: "ade1"),
        UIImage(named: "ade2"),
        UIImage(named: "ade3"),
        UIImage(named: "ade4")
    ]
}


