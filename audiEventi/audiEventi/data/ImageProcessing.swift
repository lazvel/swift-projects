//
//  ImageProcessing.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/5/21.
//

import Foundation

class ImageProcessing {
    static var sideImagesGuest = ["menu_home", "menu_calendar", "menu_coupon_eventi", "menu_impostazioni"]
    static var sideImagesPremium = ["menu_home", "menu_programma", "restaurant", "menu_luoghi_e_territorio", "ade", "menu_sondaggio", "menu_calendar", "menu_coupon_eventi", "menu_info", "menu_impostazioni"]
}
