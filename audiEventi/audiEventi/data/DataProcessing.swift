//
//  DataProcessing.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/2/21.
//

import Foundation

class DataProcessing {
    static let baseURL = "https://audieventi-api-gateway-staging.azurewebsites.net"
    static let publicEventsStr = "/audi/api/public/"
    static let langsURL = "http://mobileapp-coll.engds.it/AudiEventi/langs/it_IT.json"
    
    static func makeUrl(_ baseUrl: String, _ route: String) -> String {
        let fullUrl = baseUrl + route
        
        //print(fullUrl)
        return fullUrl
    }
    
    static var langsList: LangsModel?
    static var sideMenuGuest: [String]? = []
    static var sideMenuPremium: [String]? = []
    
    // Get the it langs properties
    static func fetchLangs(url: String, completion: @escaping (LangsModel?) -> Void){
            guard let apiURL = URL(string: url) else { return }
            let task = URLSession.shared.dataTask(with: apiURL){ (data, response, error) in
                if let error = error {
                    print("Error accesing IMDB Api: \(error)")
                }
                
                guard let httpResponse = response as? HTTPURLResponse,
                      (200...299).contains(httpResponse.statusCode) else {
                    print("Error with response, uninspected status code: \(response)")
                    return
                }
                
                if let data = data, case let langs = try? JSONDecoder().decode(LangsModel.self, from: data){
                    completion(langs)
                }
            }
        
            task.resume()
        }

}



