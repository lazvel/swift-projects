//
//  SideMenu.swift
//  audiEventi
//
//  Created by lazar.velimirovic on 7/5/21.
//

import Foundation
import UIKit

protocol MenuControllerDelegate {
    func didSelectMenuItem(named: SideMenuItem)
}

enum SideMenuItem: String, CaseIterable {
    case home = "Home"
    case calendar = "Calendar"
    case coupon = "Coupon"
    case settings = "Settings"
    //case food = "FoodExperience"
//    case ade = "
    
}

enum SideMenuPremiumItem: String, CaseIterable {
    case home = "Home"
    case calendar = "Calendar"
    case coupon = "Coupon"
    case settings = "Settings"
    //case food = "FoodExperience"
//    case ade = "
    
}


class MenuController: UITableViewController {
    public var delegate: MenuControllerDelegate?
    public static var isUserActivated = false
    
    // custom initializer ??
    private let menuItems: [SideMenuItem]
    private let color = UIColor(red: 33/255.0, green: 33/255.0, blue: 33/255.0, alpha: 1)
    
    init(with menuItems: [SideMenuItem]) {
        self.menuItems = menuItems
        super.init(nibName: nil, bundle: nil)
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }
    
    // Table
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        MenuController.isUserActivated ? DataProcessing.sideMenuPremium?.count ?? 0 : DataProcessing.sideMenuGuest?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        //cell.textLabel?.text = menuItems[indexPath.row].rawValue
        if MenuController.isUserActivated {
            cell.imageView?.image = UIImage(named: ImageProcessing.sideImagesPremium[indexPath.row])
//            cell.imageView?.frame = CGRect(x: 0, y: 0, width: 32, height: 32)
            cell.textLabel?.font = UIFont(name: "System", size: 22)
            cell.textLabel?.text = DataProcessing.sideMenuPremium![indexPath.row]
            
            cell.textLabel?.clipsToBounds =  true
        } else {
            cell.imageView?.image = UIImage(named: ImageProcessing.sideImagesGuest[indexPath.row])
            cell.textLabel?.text = DataProcessing.sideMenuGuest![indexPath.row]
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        // relay to delegate about menu item selection
        let selectedItem = menuItems[indexPath.row]
        delegate?.didSelectMenuItem(named: selectedItem)
    }
}
