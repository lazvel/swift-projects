//
//  ViewController.swift
//  rowFourColTwo
//
//  Created by lazar.velimirovic on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeRoundCorners()
        makePaddings()
        
    }
    @IBOutlet var markaTel: UITextField!
    @IBOutlet var kolicina: UITextField!
    @IBOutlet var imePrezime: UITextField!
    @IBOutlet var grad: UITextField!
    @IBOutlet var pttBroj: UITextField!
    @IBOutlet var posaljiBtn: UIButton!
    
    
    func makeRoundCorners() {
        markaTel.layer.cornerRadius = 25
        markaTel.layer.masksToBounds = true
        kolicina.layer.cornerRadius = 25
        kolicina.layer.masksToBounds = true
        imePrezime.layer.cornerRadius = 25
        imePrezime.layer.masksToBounds = true
        grad.layer.cornerRadius = 25
        grad.layer.masksToBounds = true
        pttBroj.layer.cornerRadius = 25
        pttBroj.layer.masksToBounds = true
        posaljiBtn.layer.cornerRadius = 25
    }
    
    func makePaddings() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.markaTel.frame.height))
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.kolicina.frame.height))
        let paddingView2 = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.imePrezime.frame.height))
        let paddingView3 = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.grad.frame.height))
        let paddingView4 = UIView(frame: CGRect(x: 0, y: 0, width: 15, height: self.pttBroj.frame.height))
        markaTel.leftView = paddingView
        markaTel.leftViewMode = UITextField.ViewMode.always
        
        kolicina.leftView = paddingView1
        kolicina.leftViewMode = UITextField.ViewMode.always
        
        kolicina.leftView = paddingView1
        kolicina.leftViewMode = UITextField.ViewMode.always

        imePrezime.leftView = paddingView2
        imePrezime.leftViewMode = UITextField.ViewMode.always

        grad.leftView = paddingView3
        grad.leftViewMode = UITextField.ViewMode.always

        pttBroj.leftView = paddingView4
        pttBroj.leftViewMode = UITextField.ViewMode.always
        
    }

}

