//
//  ViewController.swift
//  rowFourColumnOne
//
//  Created by lazar.velimirovic on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeCorners()
    }
    @IBOutlet var izaberiSlikuBtn: UIButton!
    @IBOutlet var imeTxt: UITextField!
    @IBOutlet var prezimeTxt: UITextField!
    @IBOutlet var emailTxt: UITextField!
    @IBOutlet var lozinkaTxt: UITextField!
    @IBOutlet var registracijaBtn: UIButton!
    
    func makeCorners() {
        izaberiSlikuBtn.layer.cornerRadius = 0.5 * izaberiSlikuBtn.bounds.size.width
        imeTxt.layer.cornerRadius = 17
        imeTxt.layer.masksToBounds = true
        prezimeTxt.layer.cornerRadius = 17
        prezimeTxt.layer.masksToBounds = true
        emailTxt.layer.cornerRadius = 17
        emailTxt.layer.masksToBounds = true
        lozinkaTxt.layer.cornerRadius = 17
        lozinkaTxt.layer.masksToBounds = true
        registracijaBtn.layer.cornerRadius = 15
    }
}

