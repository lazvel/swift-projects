//
//  ViewController.swift
//  rowThreeColumnOne
//
//  Created by lazar.velimirovic on 5/12/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        makeCornerRadius()
        makePaddings()
//        let emailImage = UIImage(named: "user")
//        addLeftIconTo(txtField: emailField, andImage: emailImage!)
//        let passwordImage = UIImage(named: "padlock")
//        addLeftIconTo(txtField: passField, andImage: passwordImage!)
    }
    @IBOutlet var fbBtn: UIButton!
    @IBOutlet var googleBtn: UIButton!
    @IBOutlet var emailField: UITextField!
    @IBOutlet var passField: UITextField!
    @IBOutlet var loginBtn: UIButton!
    
    // kada naucim resize slike za ove potrebe da uradim bice ubacena
    func addLeftIconTo(txtField: UITextField, andImage img: UIImage) {
        let leftImageView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: img.size.width, height: img.size.height))
        leftImageView.image = img
        txtField.leftView = leftImageView
        txtField.leftViewMode = .always
    }
    
    func makeCornerRadius() {
        fbBtn.layer.cornerRadius = 30
        fbBtn.layer.masksToBounds = true
        googleBtn.layer.cornerRadius = 30
        googleBtn.layer.masksToBounds = true
        loginBtn.layer.cornerRadius = 25
    }
    
    func makePaddings() {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: self.emailField.frame.height))
        let paddingView1 = UIView(frame: CGRect(x: 0, y: 0, width: 45, height: self.passField.frame.height))
        emailField.leftView = paddingView
        emailField.leftViewMode = UITextField.ViewMode.always
        
        passField.leftView = paddingView1
        passField.leftViewMode = UITextField.ViewMode.always
    }
}

