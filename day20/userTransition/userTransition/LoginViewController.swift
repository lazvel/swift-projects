//
//  LoginViewController.swift
//  userTransition
//
//  Created by lazar.velimirovic on 5/24/21.
//

import UIKit

class LoginViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        loginBtn.isEnabled = false
    }
    
    @IBOutlet var usernameField: UITextField!
    @IBOutlet var passwordField: UITextField!
    @IBOutlet var repeatPassField: UITextField!
    
    @IBOutlet var loginBtn: UIButton!
    @IBOutlet var forgotUsernameBtn: UIButton!
    @IBOutlet var forgotPassBtn: UIButton!
    
    // dodaj i za oba polja passworda
    @IBAction func usernameActivate(_ sender: Any) {
        if usernameField.text!.count > 0 {
            loginBtn.isEnabled = true
        } else {
            makeAlert("Username", "Username can't be empty")
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if loginBtn.isTouchInside && validate() {
            segue.destination.navigationItem.title = "User: \(usernameField.text ?? "")"
        } else if forgotUsernameBtn.isTouchInside {
            segue.destination.navigationItem.title = "Forgot Username"
        } else if forgotPassBtn.isTouchInside {
            segue.destination.navigationItem.title = "Forgot Password"
        } else {
            segue.destination.navigationItem.title = "Error - GO TO LOGIN"
        }
        
    }
    
    func validate() -> Bool {
        if usernameField.text!.count > 0 && passwordField.text == repeatPassField.text  && passwordField.text!.count > 0 && repeatPassField.text!.count > 0 {
            return true
        } else if passwordField.text != repeatPassField.text {
            makeAlert("Password", "Passwords doesn't match")
            return false
        } else {
            makeAlert("Failed", "Make sure all fields are filled")
            return false
        }
    }
    
    func makeAlert(_ title: String,_ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .red
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    

}
