//
//  Model.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

class Auto {
    var marka: String
    var model: String
    var motor: String
    var paketOpreme: String;
    var cena: String;
    var rasporedCilindara: String
    var brojVentila: String
    var precnikHodKlipa: String
    var tipUbrizgavanja: String
    var sistemOtvaranjaVentila: String
    var turbo: String
    var zapreminaMotora: String
    var KW: String
    var KS: String
    var snagaPriObrtajima: String
    var obrtniMoment: String
    var obrtniMomentPriObrtajima: String
    var stepenKompresije: String
    var tipMenjaca: String
    var brojStepeniPrenosa: String
    var pogon: String
    var duzina: String
    var sirina: String
    var visina: String
    var medjuosovinskoRastojanje: String
    var tezinaPraznogVozila: String
    var maksimalnaDozvoljenaTezina: String
    var zapreminaRezervoara: String
    var zapreminaPrtljaznika: String
    var maksZapreminaPrtljaznika: String
    var dozvoljenTovar: String
    var dozvoljenoOpterecenjeKrova: String
    var dozvoljenaTezinaPrikoliceBK: String
    var dozvoljenaTezinaPrikoliceSK12: String
    var dozvoljenaTezinaPrikoliceSK8: String
    var opterecenjeKuke: String
    var radijusOkretanja: String
    var tragTockovaNapred: String
    var tragTockovaNazad: String
    var maksimalnaBrzina: String
    var ubrzanje0_100: String
    var ubrzanje0_200: String
    var ubrzanje80_120FinalniStepen: String
    var zaustavniPut100: String
    var vremeZa400m: String
    var potrosnjaGrad: String
    var potrosnjaVGrada: String
    var kombinovanaPotrosnja: String
    var emisijaCO2: String
    var katalizator: String
    var prednjeKocnice: String
    var zadnjeKocnice: String
    var dimenzijePneumatika: String
    var prednjeVesanje: String
    var zadnjeVesanje: String
    var prednjeOpruge: String
    var zadnjeOpruge: String
    var prednjiStabilizator: String
    var zadnjiStabilizator: String
    var garancijaKorozija: String
    var garancijaMotor: String
    var euroNCAP: String
    var euroNCAPZvezdice: String
    var gorivo: String
    var brojVrata: String
    var brojSedista: String
    
    init(marka: String,
         model: String,
         motor: String,
         paketOpreme: String,
         cena: String,
         rasporedCilindara: String,
         brojVentila: String,
         precnikHodKlipa: String,
         tipUbrizgavanja: String,
         sistemOtvaranjaVentila: String,
         turbo: String,
         zapreminaMotora: String,
         KW: String,
         KS: String,
         snagaPriObrtajima: String,
         obrtniMoment: String,
         obrtniMomentPriObrtajima: String,
         stepenKompresije: String,
         tipMenjaca: String,
         brojStepeniPrenosa: String,
         pogon: String,
         duzina: String,
         sirina: String,
         visina: String,
         medjuosovinskoRastojanje: String,
         tezinaPraznogVozila: String,
         maksimalnaDozvoljenaTezina: String,
         zapreminaRezervoara: String,
         zapreminaPrtljaznika: String,
         maksZapreminaPrtljaznika: String,
         dozvoljenTovar: String,
         dozvoljenoOpterecenjeKrova: String,
         dozvoljenaTezinaPrikoliceBK: String,
         dozvoljenaTezinaPrikoliceSK12: String,
         dozvoljenaTezinaPrikoliceSK8: String,
         opterecenjeKuke: String,
         radijusOkretanja: String,
         tragTockovaNapred: String,
         tragTockovaNazad: String,
         maksimalnaBrzina: String,
         ubrzanje0_100: String,
         ubrzanje0_200: String,
         ubrzanje80_120FinalniStepen: String,
         zaustavniPut100: String,
         vremeZa400m: String,
         potrosnjaGrad: String,
         potrosnjaVGrada: String,
         kombinovanaPotrosnja: String,
         emisijaCO2: String,
         katalizator: String,
         prednjeKocnice: String,
         zadnjeKocnice: String,
         dimenzijePneumatika: String,
         prednjeVesanje: String,
         zadnjeVesanje: String,
         prednjeOpruge: String,
         zadnjeOpruge: String,
         prednjiStabilizator: String,
         zadnjiStabilizator: String,
         garancijaKorozija: String,
         garancijaMotor: String,
         euroNCAP: String,
         euroNCAPZvezdice: String,
         gorivo: String,
         brojVrata: String,
         brojSedista: String) {
            self.marka = marka
            self.model = model
            self.motor = motor
            self.paketOpreme = paketOpreme
            self.cena = cena
            self.rasporedCilindara = rasporedCilindara
            self.brojVentila = brojVentila
            self.precnikHodKlipa = precnikHodKlipa
            self.tipUbrizgavanja = tipUbrizgavanja
            self.sistemOtvaranjaVentila = sistemOtvaranjaVentila
            self.turbo = turbo
            self.zapreminaMotora = zapreminaMotora
            self.KW = KW
            self.KS = KS
            self.snagaPriObrtajima = snagaPriObrtajima
            self.obrtniMoment = obrtniMoment
            self.obrtniMomentPriObrtajima = obrtniMomentPriObrtajima
            self.stepenKompresije = stepenKompresije
            self.tipMenjaca = tipMenjaca
            self.brojStepeniPrenosa = brojStepeniPrenosa
            self.pogon = pogon
            self.duzina = duzina
            self.sirina = sirina
            self.visina = visina
            self.medjuosovinskoRastojanje = medjuosovinskoRastojanje
            self.tezinaPraznogVozila = tezinaPraznogVozila
            self.maksimalnaDozvoljenaTezina = maksimalnaDozvoljenaTezina
            self.zapreminaRezervoara = zapreminaRezervoara
            self.zapreminaPrtljaznika = zapreminaPrtljaznika
            self.maksZapreminaPrtljaznika = maksZapreminaPrtljaznika
            self.dozvoljenTovar = dozvoljenTovar
            self.dozvoljenoOpterecenjeKrova = dozvoljenoOpterecenjeKrova
            self.dozvoljenaTezinaPrikoliceBK = dozvoljenaTezinaPrikoliceBK
            self.dozvoljenaTezinaPrikoliceSK12 = dozvoljenaTezinaPrikoliceSK12
            self.dozvoljenaTezinaPrikoliceSK8 = dozvoljenaTezinaPrikoliceSK8
            self.opterecenjeKuke = opterecenjeKuke
            self.radijusOkretanja = radijusOkretanja
            self.tragTockovaNapred = tragTockovaNapred
            self.tragTockovaNazad = tragTockovaNazad
            self.maksimalnaBrzina = maksimalnaBrzina
            self.ubrzanje0_100 = ubrzanje0_100
            self.ubrzanje0_200 = ubrzanje0_200
            self.ubrzanje80_120FinalniStepen = ubrzanje80_120FinalniStepen
            self.zaustavniPut100 = zaustavniPut100
            self.vremeZa400m = vremeZa400m
            self.potrosnjaGrad = potrosnjaGrad
            self.potrosnjaVGrada = potrosnjaVGrada
            self.kombinovanaPotrosnja = kombinovanaPotrosnja
            self.emisijaCO2 = emisijaCO2
            self.katalizator = katalizator
            self.prednjeKocnice = prednjeKocnice
            self.zadnjeKocnice = zadnjeKocnice
            self.dimenzijePneumatika = dimenzijePneumatika
            self.prednjeVesanje = prednjeVesanje
            self.zadnjeVesanje = zadnjeVesanje
            self.prednjeOpruge = prednjeOpruge
            self.zadnjeOpruge = zadnjeOpruge
            self.prednjiStabilizator = prednjiStabilizator
            self.zadnjiStabilizator = zadnjiStabilizator
            self.garancijaKorozija = garancijaKorozija
            self.garancijaMotor = garancijaMotor
            self.euroNCAP = euroNCAP
            self.euroNCAPZvezdice = euroNCAPZvezdice
            self.gorivo = gorivo
            self.brojVrata = brojVrata
            self.brojSedista = brojSedista
    }
}



