//
//  ModelsVC.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit



class ModelsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var model = ""
    var models: [Auto] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.collectionViewLayout = UICollectionViewFlowLayout()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print(models)
        print("--------------------------")
        //print(poster)
        showPosters()
    }
    
    @IBOutlet var posterImg: UIImageView!
    @IBOutlet var collectionView: UICollectionView!
    
    
    func showPosters() {
        posterImg.image = UIImage(named: model)
       
    }
    
    // MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return models.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCustomCell", for: indexPath) as! ModelsViewCell
        cell.layer.borderWidth = 2
        cell.layer.borderColor = UIColor.red.cgColor
        cell.layer.cornerRadius = 12
        
        cell.modelSlika.image = UIImage(named: model)
        cell.modelLabela.text = models[indexPath.row].model
        cell.cenaLabela.text = models[indexPath.row].cena + "€"
        
        return cell
    }
    

    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCarDetails" {
            let controller = segue.destination as! ModelDetailsVC

            let cell = sender as! UICollectionViewCell
            
            
            let indexPath = self.collectionView!.indexPath(for: cell)
            let selectedData = models[(indexPath?.row)!]
            controller.selectedModel = selectedData
            controller.markica = models[(indexPath?.row)!].marka
            
//            if let destination = segue.destination as? ModelDetailsVC, let index =
//                collectionView.indexPathsForSelectedItems?.first {
//                destination.selectedModel = cars[index.row]
//                destination.markica = model
//
//            }

        }
    }
    
}

//  MARK: Collection View Size
extension ModelsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let frameSize = collectionView.frame.size
        let size = (frameSize.width - 64.0) / 2.0 // 27 px on both side, and within, there is 10 px gap.
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }
}

