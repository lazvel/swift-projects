//
//  HomeVC.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

class HomeVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    

}
