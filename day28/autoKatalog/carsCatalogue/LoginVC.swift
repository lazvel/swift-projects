//
//  ViewController.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

class LoginVC: UIViewController {

    let data: DataProcessing = DataProcessing()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginBtn.isEnabled = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if data.checkUser() {
            performSegue(withIdentifier: "homeScreen", sender: nil)
        }
    }
    
    @IBOutlet var username: UITextField!
    @IBOutlet var password: UITextField!
    @IBOutlet var loginBtn: UIButton!
    
    @IBAction func enableBtn(_ sender: Any) {
        loginBtn.isEnabled = true
    }
    
    // MARK: Prepare Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if loginBtn.isTouchInside && validate() {
            segue.destination.navigationItem.title = "Home"
        }
    }
    
    // MARK: String validation User and Password
    func validate() -> Bool {
        if username.text == data.user && password.text == data.pass {
            return true
        } else if password.text != data.pass {
            makeAlert("Password", "Password is not right")
            return false
        } else {
            makeAlert("Failed", "Make sure all fields are filled")
            return false
        }
    }
    
    func makeAlert(_ title: String,_ message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .red
        alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertAction.Style.default, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}

