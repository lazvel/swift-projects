//
//  ModelsTry.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/31/21.
//

import UIKit

class ModelsTry: UIViewController {

    var model: Auto = Auto(marka: "", model: "", motor: "", paketOpreme: "", cena: "", rasporedCilindara: "", brojVentila: "", precnikHodKlipa: "", tipUbrizgavanja: "", sistemOtvaranjaVentila: "", turbo: "", zapreminaMotora: "", KW: "", KS: "", snagaPriObrtajima: "", obrtniMoment: "", obrtniMomentPriObrtajima: "", stepenKompresije: "", tipMenjaca: "", brojStepeniPrenosa: "", pogon: "", duzina: "", sirina: "", visina: "", medjuosovinskoRastojanje: "", tezinaPraznogVozila: "", maksimalnaDozvoljenaTezina: "", zapreminaRezervoara: "", zapreminaPrtljaznika: "", maksZapreminaPrtljaznika: "", dozvoljenTovar: "", dozvoljenoOpterecenjeKrova: "", dozvoljenaTezinaPrikoliceBK: "", dozvoljenaTezinaPrikoliceSK12: "", dozvoljenaTezinaPrikoliceSK8: "", opterecenjeKuke: "", radijusOkretanja: "", tragTockovaNapred: "", tragTockovaNazad: "", maksimalnaBrzina: "", ubrzanje0_100: "", ubrzanje0_200: "", ubrzanje80_120FinalniStepen: "", zaustavniPut100: "", vremeZa400m: "", potrosnjaGrad: "", potrosnjaVGrada: "", kombinovanaPotrosnja: "", emisijaCO2: "", katalizator: "", prednjeKocnice: "", zadnjeKocnice: "", dimenzijePneumatika: "", prednjeVesanje: "", zadnjeVesanje: "", prednjeOpruge: "", zadnjeOpruge: "", prednjiStabilizator: "", zadnjiStabilizator: "", garancijaKorozija: "", garancijaMotor: "", euroNCAP: "", euroNCAPZvezdice: "", gorivo: "", brojVrata: "", brojSedista: "")
    //var poster: LogoProducts = LogoProducts(name: "", logo: "")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(model)
        print("-------------")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        nameLabel.text = String(model.marka)
        posterImg.image = UIImage(named: model.marka)
        //modelLabel.text = model.model[]
        //print(model.marka)
    }
    
    @IBOutlet var posterImg: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var modelLabel: UILabel!

}
