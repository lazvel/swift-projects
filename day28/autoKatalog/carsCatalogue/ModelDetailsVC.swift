//
//  ModelDetailsVC.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

class ModelDetailsVC: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var selectedModel: Auto? = nil
    
    var carDetalji: [String?] {
        get {
            return [selectedModel?.marka, selectedModel?.model, selectedModel?.motor, selectedModel?.paketOpreme, selectedModel?.cena, selectedModel?.rasporedCilindara, selectedModel?.brojVentila, selectedModel?.precnikHodKlipa, selectedModel?.tipUbrizgavanja, selectedModel?.sistemOtvaranjaVentila, selectedModel?.turbo, selectedModel?.zapreminaMotora, selectedModel?.KW, selectedModel?.KS, selectedModel?.snagaPriObrtajima, selectedModel?.obrtniMoment, selectedModel?.obrtniMomentPriObrtajima, selectedModel?.stepenKompresije, selectedModel?.tipMenjaca, selectedModel?.brojStepeniPrenosa, selectedModel?.pogon, selectedModel?.duzina, selectedModel?.sirina, selectedModel?.visina, selectedModel?.medjuosovinskoRastojanje, selectedModel?.tezinaPraznogVozila, selectedModel?.maksimalnaDozvoljenaTezina, selectedModel?.zapreminaRezervoara, selectedModel?.zapreminaPrtljaznika, selectedModel?.maksZapreminaPrtljaznika, selectedModel?.dozvoljenTovar, selectedModel?.dozvoljenoOpterecenjeKrova, selectedModel?.dozvoljenaTezinaPrikoliceBK, selectedModel?.dozvoljenaTezinaPrikoliceSK12, selectedModel?.dozvoljenaTezinaPrikoliceSK8, selectedModel?.opterecenjeKuke, selectedModel?.radijusOkretanja, selectedModel?.tragTockovaNapred, selectedModel?.tragTockovaNazad, selectedModel?.maksimalnaBrzina, selectedModel?.ubrzanje0_100, selectedModel?.ubrzanje0_200, selectedModel?.ubrzanje80_120FinalniStepen, selectedModel?.zaustavniPut100, selectedModel?.vremeZa400m, selectedModel?.potrosnjaGrad, selectedModel?.potrosnjaVGrada, selectedModel?.kombinovanaPotrosnja, selectedModel?.emisijaCO2, selectedModel?.katalizator, selectedModel?.prednjeKocnice, selectedModel?.zadnjeKocnice, selectedModel?.dimenzijePneumatika, selectedModel?.prednjeVesanje, selectedModel?.zadnjeVesanje, selectedModel?.prednjeOpruge, selectedModel?.zadnjeOpruge, selectedModel?.prednjiStabilizator, selectedModel?.zadnjiStabilizator, selectedModel?.garancijaKorozija, selectedModel?.garancijaMotor, selectedModel?.euroNCAP, selectedModel?.euroNCAPZvezdice, selectedModel?.gorivo, selectedModel?.brojVrata, selectedModel?.brojSedista]
        }
        set {
            self.carDetalji = [selectedModel?.marka, selectedModel?.model, selectedModel?.motor, selectedModel?.paketOpreme, selectedModel?.cena, selectedModel?.rasporedCilindara, selectedModel?.brojVentila, selectedModel?.precnikHodKlipa, selectedModel?.tipUbrizgavanja, selectedModel?.sistemOtvaranjaVentila, selectedModel?.turbo, selectedModel?.zapreminaMotora, selectedModel?.KW, selectedModel?.KS, selectedModel?.snagaPriObrtajima, selectedModel?.obrtniMoment, selectedModel?.obrtniMomentPriObrtajima, selectedModel?.stepenKompresije, selectedModel?.tipMenjaca, selectedModel?.brojStepeniPrenosa, selectedModel?.pogon, selectedModel?.duzina, selectedModel?.sirina, selectedModel?.visina, selectedModel?.medjuosovinskoRastojanje, selectedModel?.tezinaPraznogVozila, selectedModel?.maksimalnaDozvoljenaTezina, selectedModel?.zapreminaRezervoara, selectedModel?.zapreminaPrtljaznika, selectedModel?.maksZapreminaPrtljaznika, selectedModel?.dozvoljenTovar, selectedModel?.dozvoljenoOpterecenjeKrova, selectedModel?.dozvoljenaTezinaPrikoliceBK, selectedModel?.dozvoljenaTezinaPrikoliceSK12, selectedModel?.dozvoljenaTezinaPrikoliceSK8, selectedModel?.opterecenjeKuke, selectedModel?.radijusOkretanja, selectedModel?.tragTockovaNapred, selectedModel?.tragTockovaNazad, selectedModel?.maksimalnaBrzina, selectedModel?.ubrzanje0_100, selectedModel?.ubrzanje0_200, selectedModel?.ubrzanje80_120FinalniStepen, selectedModel?.zaustavniPut100, selectedModel?.vremeZa400m, selectedModel?.potrosnjaGrad, selectedModel?.potrosnjaVGrada, selectedModel?.kombinovanaPotrosnja, selectedModel?.emisijaCO2, selectedModel?.katalizator, selectedModel?.prednjeKocnice, selectedModel?.zadnjeKocnice, selectedModel?.dimenzijePneumatika, selectedModel?.prednjeVesanje, selectedModel?.zadnjeVesanje, selectedModel?.prednjeOpruge, selectedModel?.zadnjeOpruge, selectedModel?.prednjiStabilizator, selectedModel?.zadnjiStabilizator, selectedModel?.garancijaKorozija, selectedModel?.garancijaMotor, selectedModel?.euroNCAP, selectedModel?.euroNCAPZvezdice, selectedModel?.gorivo, selectedModel?.brojVrata, selectedModel?.brojSedista]
        }
    }
    
    var markica = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        logoImg.image = UIImage(named: markica)
        cenaLabela.text = selectedModel!.cena + "€"
        print("\(selectedModel?.cena ?? "nema")")
    }
    
    
    @IBOutlet var logoImg: UIImageView!
    @IBOutlet var cenaLabela: UILabel!
    @IBOutlet var tableVieww: UITableView!

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return temp
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "detailsCell", for: indexPath) as! DetailsTableCell
        cell.valueLabela?.text = carDetalji[indexPath.row]
        cell.stringLabela.text = DataProcessing.properties[indexPath.row]
        
        return cell
    }
    
    @IBOutlet var showHideBtn: UIButton!
    
    var temp = 10
    @IBAction func showHide(_ sender: Any) {
        if temp == 10 {
            temp = carDetalji.count
            showHideBtn.setTitle("Show less", for: .normal)
            
            tableVieww.reloadData()
        } else {
            temp = 10
            showHideBtn.setTitle("Show more", for: .normal)
            tableVieww.reloadData()
        }
    }
    
}
