//
//  LogoCollectionCell.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

class LogoCollectionCell: UICollectionViewCell {
    
    @IBOutlet var logo: UIImageView!
    @IBOutlet var logoName: UILabel!
}
