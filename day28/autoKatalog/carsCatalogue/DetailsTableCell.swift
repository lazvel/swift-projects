//
//  DetailsTableCell.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 6/1/21.
//

import UIKit

class DetailsTableCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        
        stringLabela.adjustsFontSizeToFitWidth = autoresizesSubviews
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBOutlet var stringLabela: UILabel!
    @IBOutlet var valueLabela: UILabel!
    
    
    

}
