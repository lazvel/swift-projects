//
//  ProductsVC.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 5/30/21.
//

import UIKit

var markeSet:Set<String> = []
var markeArray: [String] = []

class ProductsVC: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UISearchBarDelegate {
    let data: DataProcessing = DataProcessing()
    
    var markeSearch = [String]()
    var searching = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        collectionView.collectionViewLayout = UICollectionViewFlowLayout()
        markeSearch = markeArray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DataProcessing.readFromFile()
        DataProcessing.parseCarsFromModel()
    }
    
    @IBOutlet var collectionView: UICollectionView!
    @IBOutlet var searchBarBtn: UIButton!
    
    
    // MARK: Collection View
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if searching {
            print("markeSearch \(markeSearch)")
            return markeSearch.count
            
        } else {
            print("markeArray \(markeArray)")
            return markeArray.count
        }
        //markeArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "myCollectionCell", for: indexPath) as! LogoCollectionCell
        if searching {
            cell.logo.image = UIImage(named: markeSearch[indexPath.row])
            cell.logoName.text = markeSearch[indexPath.row]
        } else {
            cell.logo.image = UIImage(named: markeArray[indexPath.row])
            cell.logoName.text = markeArray[indexPath.row]
        }
        cell.layer.borderWidth = 2
        cell.layer.borderColor = UIColor.blue.cgColor
        cell.layer.cornerRadius = 12
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetails" {
//            if let destination = segue.destination as? ModelsVC, let index =
//                collectionView.indexPathsForSelectedItems?.first {
//                destination.model = markeArray[index.row]
//                destination.models = cars.filter{$0.marka == markeArray[index.row]}
//                destination.autoDetails = carDetails.filter{$0.model == cars[index.row].model}
            let controller = segue.destination as! ModelsVC

            let cell = sender as! UICollectionViewCell
            
            let indexPath = self.collectionView!.indexPath(for: cell)
            
            if searching {
                let selectedData = markeSearch[(indexPath?.row)!]
                controller.model = selectedData
                controller.models = DataProcessing.cars.filter{$0.marka == markeSearch[(indexPath?.row)!]}
            } else {
                let selectedData = markeArray[(indexPath?.row)!]
                controller.model = selectedData
                controller.models = DataProcessing.cars.filter{$0.marka == markeArray[(indexPath?.row)!]}
            }
                
//            let selectedData = markeSearch[(indexPath?.row)!]
//            controller.model = selectedData
//            controller.models = cars.filter{$0.model == markeSearch[(indexPath?.row)!]}
            
        }
    }
    
   @IBOutlet var searchBar: UISearchBar!
    
   
    @IBAction func searchClicked(_ sender: Any) {
        searchBarSearchButtonClicked(searchBar)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        markeSearch = markeArray.filter({$0.prefix(searchText.lowercased().count) == searchText})
//       print("Marke \(markeSearch)")
//        collectionView.reloadData()

        if searchText == "" {
            markeSearch = markeArray
            collectionView.reloadData()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        markeSearch = markeArray.filter({$0.prefix(searchBar.text!.lowercased().count) == searchBar.text!})
        print(searchBar.text!)
        //print(markeSearch)
        if markeSearch.count == 1 {
            collectionView.reloadData()
            return
        } else {
            for car in DataProcessing.cars {
                if car.model.lowercased().contains((searchBar.text?.lowercased())!) {
                    if !markeSearch.contains(car.marka) {
                        markeSearch.append(car.marka)
                    }
                }
            }
        }
        searching = true
        
        collectionView.reloadData()
    }
    
    
//    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
//        searching = false
//        searchBar.text = ""
//
//        collectionView.reloadData()
//    }

}

// MARK: Collection View Size
extension ProductsVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (view.frame.width - 60) / 2
        let height = width
        return CGSize(width: width, height: height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 10
       }
}
