//
//  DataProcessing.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 6/3/21.
//

import UIKit

class DataProcessing {
    // MARK: Parsing Model

    static var fileContent: String?
    static let fileName: String = "model2"
    static let fileExtension: String = "csv"
    
    static var cars: [Auto] = []

    static func readFromFile() {
        let path = Bundle.main.path(forResource: fileName, ofType: fileExtension)
        fileContent = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        
        if let fileContent = fileContent {
            //print(fileContent)
            //print("--------------------")
        }
    }

    static var carModels: [String] = []

    static func parseCarsFromModel() {
        let data = DataProcessing()
        if let fileContent = fileContent {
            let lineByArray = fileContent.split(separator: "\n")
            DataProcessing.cars = []
            
            //carDetails = []
            
            for row in lineByArray {
                let lineToArray = row.split(separator: ";")
                
                let marka = String(lineToArray[0])
                let model = String(lineToArray[1])
                let motor = String(lineToArray[2])
                let paketOpreme = String(lineToArray[3])
                let cena = String(lineToArray[4])
                let rasporedCilindara = String(lineToArray[5])
                let brojVentila = String(lineToArray[6])
                let precnikHodKlipa = String(lineToArray[7])
                let tipUbrizgavanja = String(lineToArray[8])
                let sistemOtvaranjaVentila = String(lineToArray[9])
                let turbo = String(lineToArray[10])
                let zapreminaMotora = String(lineToArray[11])
                let KW = String(lineToArray[12])
                let KS = String(lineToArray[13])
                let snagaPriObrtajima = String(lineToArray[14])
                let obrtniMoment = String(lineToArray[15])
                let obrtniMomentPriObrtajima = String(lineToArray[16])
                let stepenKompresije = String(lineToArray[17])
                let tipMenjaca = String(lineToArray[18])
                let brojStepeniPrenosa = String(lineToArray[19])
                let pogon = String(lineToArray[20])
                let duzina = String(lineToArray[21])
                let sirina = String(lineToArray[22])
                let visina = String(lineToArray[23])
                let medjuosovinskoRastojanje = String(lineToArray[24])
                let tezinaPraznogVozila = String(lineToArray[25])
                let maksimalnaDozvoljenaTezina = String(lineToArray[26])
                let zapreminaRezervoara = String(lineToArray[27])
                let zapreminaPrtljaznika = String(lineToArray[28])
                let maksZapreminaPrtljaznika = String(lineToArray[29])
                let dozvoljenTovar = String(lineToArray[30])
                let dozvoljenoOpterecenjeKrova = String(lineToArray[31])
                let dozvoljenaTezinaPrikoliceBK = String(lineToArray[32])
                let dozvoljenaTezinaPrikoliceSK12 = String(lineToArray[33])
                let dozvoljenaTezinaPrikoliceSK8 = String(lineToArray[34])
                let opterecenjeKuke = String(lineToArray[35])
                let radijusOkretanja = String(lineToArray[36])
                let tragTockovaNapred = String(lineToArray[37])
                let tragTockovaNazad = String(lineToArray[38])
                let maksimalnaBrzina = String(lineToArray[39])
                let ubrzanje0_100 = String(lineToArray[40])
                let ubrzanje0_200 = String(lineToArray[41])
                let ubrzanje80_120FinalniStepen = String(lineToArray[42])
                let zaustavniPut100 = String(lineToArray[43])
                let vremeZa400m = String(lineToArray[44])
                let potrosnjaGrad = String(lineToArray[45])
                let potrosnjaVGrada = String(lineToArray[46])
                let kombinovanaPotrosnja = String(lineToArray[47])
                let emisijaCO2 = String(lineToArray[48])
                let katalizator = String(lineToArray[49])
                let prednjeKocnice = String(lineToArray[50])
                let zadnjeKocnice = String(lineToArray[51])
                let dimenzijePneumatika = String(lineToArray[52])
                let prednjeVesanje = String(lineToArray[53])
                let zadnjeVesanje = String(lineToArray[54])
                let prednjeOpruge = String(lineToArray[55])
                let zadnjeOpruge = String(lineToArray[56])
                let prednjiStabilizator = String(lineToArray[57])
                let zadnjiStabilizator = String(lineToArray[58])
                let garancijaKorozija = String(lineToArray[59])
                let garancijaMotor = String(lineToArray[60])
                let euroNCAP = String(lineToArray[61])
                let euroNCAPZvezdice = String(lineToArray[62])
                let gorivo = String(lineToArray[63])
                let brojVrata = String(lineToArray[64])
                let brojSedista = String(lineToArray[65])
                
                let newCar = Auto(marka: marka, model: model, motor: motor, paketOpreme: paketOpreme, cena: cena, rasporedCilindara: rasporedCilindara, brojVentila: brojVentila, precnikHodKlipa: precnikHodKlipa, tipUbrizgavanja: tipUbrizgavanja, sistemOtvaranjaVentila: sistemOtvaranjaVentila, turbo: turbo, zapreminaMotora: zapreminaMotora, KW: KW, KS: KS, snagaPriObrtajima: snagaPriObrtajima, obrtniMoment: obrtniMoment, obrtniMomentPriObrtajima: obrtniMomentPriObrtajima, stepenKompresije: stepenKompresije, tipMenjaca: tipMenjaca, brojStepeniPrenosa: brojStepeniPrenosa, pogon: pogon, duzina: duzina, sirina: sirina, visina: visina, medjuosovinskoRastojanje: medjuosovinskoRastojanje, tezinaPraznogVozila: tezinaPraznogVozila, maksimalnaDozvoljenaTezina: maksimalnaDozvoljenaTezina, zapreminaRezervoara: zapreminaRezervoara, zapreminaPrtljaznika: zapreminaPrtljaznika, maksZapreminaPrtljaznika: maksZapreminaPrtljaznika, dozvoljenTovar: dozvoljenTovar, dozvoljenoOpterecenjeKrova: dozvoljenoOpterecenjeKrova, dozvoljenaTezinaPrikoliceBK: dozvoljenaTezinaPrikoliceBK, dozvoljenaTezinaPrikoliceSK12: dozvoljenaTezinaPrikoliceSK12, dozvoljenaTezinaPrikoliceSK8: dozvoljenaTezinaPrikoliceSK8, opterecenjeKuke: opterecenjeKuke, radijusOkretanja: radijusOkretanja, tragTockovaNapred: tragTockovaNapred, tragTockovaNazad: tragTockovaNazad, maksimalnaBrzina: maksimalnaBrzina, ubrzanje0_100: ubrzanje0_100, ubrzanje0_200: ubrzanje0_200, ubrzanje80_120FinalniStepen: ubrzanje80_120FinalniStepen, zaustavniPut100: zaustavniPut100, vremeZa400m: vremeZa400m, potrosnjaGrad: potrosnjaGrad, potrosnjaVGrada: potrosnjaVGrada, kombinovanaPotrosnja: kombinovanaPotrosnja, emisijaCO2: emisijaCO2, katalizator: katalizator, prednjeKocnice: prednjeKocnice, zadnjeKocnice: zadnjeKocnice, dimenzijePneumatika: dimenzijePneumatika, prednjeVesanje: prednjeVesanje, zadnjeVesanje: zadnjeVesanje, prednjeOpruge: prednjeOpruge, zadnjeOpruge: zadnjeOpruge, prednjiStabilizator: prednjiStabilizator, zadnjiStabilizator: zadnjiStabilizator, garancijaKorozija: garancijaKorozija, garancijaMotor: garancijaMotor, euroNCAP: euroNCAP, euroNCAPZvezdice: euroNCAPZvezdice, gorivo: gorivo, brojVrata: brojVrata, brojSedista: brojSedista)
                
                carModels.append(model)
                markeSet.insert(marka)
                markeArray = Array(markeSet).sorted()
                DataProcessing.cars.append(newCar)
                //carDetails.append(newCarDetails)
            }
        }
        
        print("Marke: \(markeArray)")
        //print(cars)
        print("------------------------------")
        //print("\(carModels)")
        //print(carDetails)
    }
    
    static var properties = ["Marka", "Model", "Motor", "Paket opreme", "Cena", "Raspored cilindara", "Broj ventila", "Precnik Hod Klipa", "Tip ubrizgavanja", "Sistem otvaranja ventila", "Turbo", "Zapremina motora", "KW", "KS", "Snaga pri obrtajima", "Obrtni moment", "Obrtni moment pri obrtajima", "Stepen kompresije", "Tip menjaca", "Broj stepeni prenosa", "Pogon", "Duzina", "Sirina", "Visina", "Medjuosovinsko rastojanje", "Tezina praznog Vozila", "Maksimalna dozvoljena tezina", "Zapremina rezervoara", "Zapremina prtljaznika", "Maks zapremina prtljaznika", "Dozvoljen tovar", "Dozvoljeno opterecenje krova", "Dozvoljena tezina prikolice BK", "Dozvoljena Tezina Prikolice SK12", "Dozvoljena tezina prikolice SK8", "Opterecenje kuke", "Radijus okretanja", "Trag tockova napred", "Trag tockova nazad", "Maksimalna brzina", "Ubrzanje 0-100", "Ubrzanje 0-200", "Ubrzanje 80-120 finalni stepen", "Zaustavni put 100m", "Vreme Za 400m", "Potrosnja Grad", "Potrosnja VGrada", "Kombinovana Potrosnja", "Emisija CO2", "Katalizator", "Prednje kocnice", "Zadnje kocnice", "Dimenzije Pneumatika", "Prednje Vesanje", "Zadnje vesanje", "Prednje opruge", "Zadnje opruge", "Prednji stabilizator", "Zadnji stabilizator", "Garancija korozija", "Garancija motor", "Euro NCAP", "Euro NCAP Zvezdice", "Gorivo", "Broj Vrata", "Broj Sedista"]
    
    
    // MARK: User Check
    let user = "user"
    let pass = "pass"

    var username = "user"
    var password = "pass"

    func checkUser() -> Bool {
        if user == username && pass == password {
            return true
        } else {
            return false
        }
    }
}
