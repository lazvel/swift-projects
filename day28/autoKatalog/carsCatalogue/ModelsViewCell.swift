//
//  ModelsViewCell.swift
//  carsCatalogue
//
//  Created by lazar.velimirovic on 6/1/21.
//

import UIKit

class ModelsViewCell: UICollectionViewCell {
    
    @IBOutlet var modelSlika: UIImageView!
    @IBOutlet var modelLabela: UILabel!
    @IBOutlet var cenaLabela: UILabel!
}
